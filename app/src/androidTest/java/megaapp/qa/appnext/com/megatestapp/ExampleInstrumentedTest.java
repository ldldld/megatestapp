package megaapp.qa.appnext.com.megatestapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.web.webdriver.Locator;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import megaapp.qa.appnext.com.megatestapp.androidsdk.AdBuilder;
import megaapp.qa.appnext.com.megatestapp.androidsdk.FullScreenNative;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.web.model.Atoms.script;
import static android.support.test.espresso.web.sugar.Web.onWebView;
import static android.support.test.espresso.web.webdriver.DriverAtoms.findElement;
import static android.support.test.espresso.web.webdriver.DriverAtoms.selectFrameByIdOrName;
import static android.support.test.espresso.web.webdriver.DriverAtoms.webClick;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("megaapp.qa.appnext.com.megatestapp", appContext.getPackageName());
    }

    @Test
    public void espressoTest() throws InterruptedException {


        onView(withId(R.id.androidSDK))
                .perform(click());

        onView(withId(R.id.fullscreenActivity))
                .perform(click());


        onView(withId(R.id.fullscreenCacheDefault))
                .perform(click());


        //Thread.sleep(20000);

       //onView(withId(R.id.fullscreenShowDefault)).perform(click());
        Thread.sleep(5000);

        onView(withId(R.id.fullscreenShowDefault)).check(matches(isEnabled()));
//        if (FullScreenNative.statusValue == 1){
//            Toast.makeText(InstrumentationRegistry.getTargetContext(), "value " + FullScreenNative.statusValue, Toast.LENGTH_SHORT).show();
//        }
        onView(withId(R.id.fullscreenShowDefault)).perform(click());
        //onWebView().forceJavascriptEnabled();

        //onView(withText("Play")).check(matches(isDisplayed()));
        //onWebView().withElement(findElement(Locator.valueOf("Install"), "Install")).perform(webClick());
        Thread.sleep(8000);

       // onWebView().perform(webClick());
//onView(withText("Download")).perform(click());
//        onView(withText("Install"));

onWebView().withElement(findElement(Locator.ID, "install")).perform(webClick());

onWebView().inWindow(selectFrameByIdOrName("close"))
.withElement(findElement(Locator.ID, "close"))
.perform(webClick());



//onView(withId(R.id.install)).perform(click());
//onView(withId(R.id.med)).perform(click());
    }


}
