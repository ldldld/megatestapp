package megaapp.qa.appnext.com.megatestapp.utilities;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import static com.google.ads.AdRequest.LOGTAG;


public class LogToastUtil {
    static void logToast(Context context, String message) {
        Log.d(LOGTAG, message);

        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

}
