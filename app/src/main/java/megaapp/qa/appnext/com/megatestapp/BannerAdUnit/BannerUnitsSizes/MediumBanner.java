package megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerListener;
import com.appnext.banners.BannerView;
import com.appnext.core.AppnextError;
import com.appnext.core.ECPM;
import com.appnext.core.callbacks.OnECPMLoaded;

import megaapp.qa.appnext.com.megatestapp.banneradunit.BannerUnits;
import megaapp.qa.appnext.com.megatestapp.R;

public class MediumBanner extends AppCompatActivity implements View.OnClickListener{
    //private static final String PLACEMENT_ID2 = "e6390089-3f37-4810-af29-c8424dbf8abb";CPI
    //private static final String PLACEMENT_ID2 = "b308acb6-462b-42f0-8ef3-6ba46aba7ef0";//CPC
    //private static final String PLACEMENT_ID2 = "f1180cd3-e6ab-4f09-9072-411d2b2a1fe3";//CPC
    private static final String PLACEMENT_ID2 = "17148fe6-61a7-4667-b944-859c6a3c1464";//CPC
    //private static final String PLACEMENT_ID2 = "a82d08e9-d1f9-4bdd-8159-f463257e87b4";//CPC
    //    pid 320x50 - a82d08e9-d1f9-4bdd-8159-f463257e87b4
//    pid 320x100 - 8d5c7226-fd3c-42a8-a9d2-f8530e0a79b2
//    pid 300x250 - 562eaade-855e-48fd-b163-346272ba36eb
    private BannerView bannerViewMedium;
    private BannerAdRequest banner_requestMedium;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medium_banner);
        Button ecpmMediumBanner = findViewById(R.id.ecpmMedium);
        ecpmMediumBanner.setOnClickListener(this);

        Button customLargeBanner =  findViewById(R.id.customMedium);
        customLargeBanner.setOnClickListener(this);

        bannerViewMedium =  findViewById(R.id.mediumBanner);
        Button buttonLoadAd = findViewById(R.id.buttonLoadAd);
        buttonLoadAd.setOnClickListener(this);
        bannerViewMedium.setPlacementId(PLACEMENT_ID2);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case(R.id.buttonLoadAd):
                banner_requestMedium = new BannerAdRequest();
                bannerViewMedium.setLanguage("KO");
banner_requestMedium.setCreativeType(BannerAdRequest.TYPE_VIDEO);
banner_requestMedium.setVideoLength(BannerAdRequest.VIDEO_LENGTH_LONG);
banner_requestMedium.setAutoPlay(true);
                callBacks(bannerViewMedium);
                bannerViewMedium.loadAd(banner_requestMedium);

                break;

            case(R.id.ecpmMedium):
                bannerViewMedium.getECPM(new BannerAdRequest(), new OnECPMLoaded() {
                    @Override
                    public void ecpm(ECPM ecpm) {
                        Log.v("ecpm", "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr() + ", banner ID: " + ecpm.getBanner());
                        banner_requestMedium = new BannerAdRequest();
                        bannerViewMedium.loadAd(banner_requestMedium);
                        Toast.makeText(MediumBanner.this, "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr() + ", banner ID: " + ecpm.getBanner(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void error(String s) {
                        Log.v("ecpm", "error: " + s);
                        Toast.makeText(MediumBanner.this, "Error ecpm", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case (R.id.customMedium):
                SharedPreferences spSetters = getSharedPreferences("setters", MODE_PRIVATE);
                spSetters.edit().putString("whoAmI", "medium").apply();
                startActivity(new Intent(MediumBanner.this, BannersWithSetters.class));
                finish();
                break;
        }
    }

    private void callBacks(BannerView bannerViewMedium){
        bannerViewMedium.setBannerListener(new BannerListener(){
            @Override
            public void onError(AppnextError s) {
                super.onError(s);
                Log.e("==== crash --", s.getErrorMessage());
                Toast.makeText(MediumBanner.this, "Error " + s.getErrorMessage(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onAdLoaded(String adLoadedString) {
                super.onAdLoaded(adLoadedString);
                Log.e("==== Loaded -- ", adLoadedString);
                Toast.makeText(MediumBanner.this, "Loaded " + adLoadedString, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Log.e("==== Ad Clicked -- ", "Clicked");
                Toast.makeText(MediumBanner.this, "Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void adImpression() {
                super.adImpression();
                Log.e("==== Ad Impression -- ", "Impressed");
                Toast.makeText(MediumBanner.this, "Impressed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bannerViewMedium != null) {
            bannerViewMedium.destroy();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
//        startActivity(new Intent(MediumBanner.this, BannerUnits.class));
//        finish();
    }
}
