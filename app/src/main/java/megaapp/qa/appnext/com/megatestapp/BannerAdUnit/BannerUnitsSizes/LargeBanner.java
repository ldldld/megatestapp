package megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerListener;
import com.appnext.banners.BannerView;
import com.appnext.core.AppnextError;
import com.appnext.core.ECPM;
import com.appnext.core.callbacks.OnECPMLoaded;

import megaapp.qa.appnext.com.megatestapp.banneradunit.BannerUnits;
import megaapp.qa.appnext.com.megatestapp.R;

public class LargeBanner extends AppCompatActivity implements View.OnClickListener{

    //private static final String PLACEMENT_ID3 = "1039a40d-cd1d-4dac-acf4-6e50005e1ee9";//CPI
    private static final String PLACEMENT_ID3 = "9028a152-0577-4ae6-bb3d-c64f4fdca29a"; //CPC
//    pid 320x50 - a82d08e9-d1f9-4bdd-8159-f463257e87b4
//    pid 320x100 - 8d5c7226-fd3c-42a8-a9d2-f8530e0a79b2
//    pid 300x250 - 562eaade-855e-48fd-b163-346272ba36eb
    private BannerView bannerViewLarge;
    private BannerAdRequest banner_requestLarge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_banner);

        Button ecpmLargeBanner = findViewById(R.id.ecpmLarge);
        ecpmLargeBanner.setOnClickListener(this);

        Button customLargeBanner =  findViewById(R.id.customLarge);
        customLargeBanner.setOnClickListener(this);

        bannerViewLarge =  findViewById(R.id.largeBanner);
        Button buttonLoadAd = findViewById(R.id.buttonLoadAd);
        buttonLoadAd.setOnClickListener(this);
        bannerViewLarge.setPlacementId(PLACEMENT_ID3);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case(R.id.buttonLoadAd):
                banner_requestLarge = new BannerAdRequest();
                bannerViewLarge.setLanguage("KO");
                callBacks(bannerViewLarge);
                bannerViewLarge.loadAd(banner_requestLarge);
                break;

            case(R.id.ecpmLarge):
                bannerViewLarge.getECPM(new BannerAdRequest(), new OnECPMLoaded() {
                    @Override
                    public void ecpm(ECPM ecpm) {
                        Log.v("ecpm", "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr() + ", banner ID: " + ecpm.getBanner());
                        banner_requestLarge = new BannerAdRequest();
                        bannerViewLarge.loadAd(banner_requestLarge);
                        Toast.makeText(LargeBanner.this, "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr() + ", banner ID: " + ecpm.getBanner(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void error(String s) {
                        Log.v("ecpm", "error: " + s);
                        Toast.makeText(LargeBanner.this, "Error ecpm", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case (R.id.customLarge):
                SharedPreferences spSetters = getSharedPreferences("setters", MODE_PRIVATE);
                spSetters.edit().putString("whoAmI", "large").apply();
                startActivity(new Intent(LargeBanner.this, BannersWithSetters.class));
                finish();
                break;
        }
    }

    private void callBacks(BannerView bannerViewLarge){
        bannerViewLarge.setBannerListener(new BannerListener(){
            @Override
            public void onError(AppnextError s) {
                super.onError(s);
                Log.e("==== Error -- ", s.getErrorMessage());
                Toast.makeText(LargeBanner.this, "Error " + s.getErrorMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded(String adLoadedString) {
                super.onAdLoaded(adLoadedString);
                Log.e("==== Loaded -- ", adLoadedString);
                Toast.makeText(LargeBanner.this, "Loaded " + adLoadedString, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Log.e("==== Ad Clicked -- ", "Clicked");
                Toast.makeText(LargeBanner.this, "Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void adImpression() {
                super.adImpression();
                Log.e("==== Ad Impression -- ", "Impressed");
                Toast.makeText(LargeBanner.this, "Impressed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bannerViewLarge != null) {
            bannerViewLarge.destroy();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
//        startActivity(new Intent(LargeBanner.this, BannerUnits.class));
//        finish();
    }
}
