package megaapp.qa.appnext.com.megatestapp.androidsdk;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import megaapp.qa.appnext.com.megatestapp.configuration.SdkFsInterRewardConfiguration;
import megaapp.qa.appnext.com.megatestapp.MainActivity;
import megaapp.qa.appnext.com.megatestapp.R;

public class AndroidSdkMainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_sdk_main);

//        Fabric.with(this, new Crashlytics());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        Button fullscreen = findViewById(R.id.fullscreenActivity);
        Button rewarded = findViewById(R.id.rewardedActivity);
        Button interstitial = findViewById(R.id.interstitialActivity);
        Button interstitialMultiProcces = findViewById(R.id.interstitialActivityMulti);
//        cacheVideo = findViewById(R.id.cache_video_switch);
//        streamingMode = findViewById(R.id.streaming_mode_switch);

        fullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AndroidSdkMainActivity.this, SdkFsInterRewardConfiguration.class);
                intent.putExtra("origin", "fullScreen");
                startActivity(intent);
                finish();
            }
        });

        rewarded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AndroidSdkMainActivity.this, SdkFsInterRewardConfiguration.class);
                intent.putExtra("origin", "rewarded");
                startActivity(intent);
                finish();
            }
        });

        interstitial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AndroidSdkMainActivity.this, SdkFsInterRewardConfiguration.class);
                intent.putExtra("origin", "interstitial");
                startActivity(intent);
                finish();
            }
        });

        interstitialMultiProcces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AndroidSdkMainActivity.this, MultiProccesActivity.class);
                intent.putExtra("origin", "interstitial");
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AndroidSdkMainActivity.this, MainActivity.class));
        finish();
    }
}
