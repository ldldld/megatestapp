package megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerListener;
import com.appnext.banners.BannerView;
import com.appnext.core.AppnextError;
import com.appnext.core.ECPM;
import com.appnext.core.callbacks.OnECPMLoaded;

import megaapp.qa.appnext.com.megatestapp.banneradunit.BannerUnits;
import megaapp.qa.appnext.com.megatestapp.configuration.BannersConfig;
import megaapp.qa.appnext.com.megatestapp.R;

public class SmallBanner extends AppCompatActivity implements View.OnClickListener{
    private BannerView bannerViewSmall;
    private BannerAdRequest banner_requestSmall;

    //private static final String PLACEMENT_ID1 = "4c8fd5c8-0ed1-411a-95fd-709d88d4fda7";//CPI
    //private static final String PLACEMENT_ID1 = "b308acb6-462b-42f0-8ef3-6ba46aba7ef0"; //CPC
    private final String PLACEMENT_ID1 = "a82d08e9-d1f9-4bdd-8159-f463257e87b4"; //CPC
//    pid 320x50 - a82d08e9-d1f9-4bdd-8159-f463257e87b4
//    pid 320x100 - 8d5c7226-fd3c-42a8-a9d2-f8530e0a79b2
//    pid 300x250 - 562eaade-855e-48fd-b163-346272ba36eb

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_small_banner);
        Button buttonLoadAd = findViewById(R.id.buttonLoadAd);
        buttonLoadAd.setOnClickListener(this);
        bannerViewSmall =  findViewById(R.id.smallBanner);
        bannerViewSmall.setPlacementId(PLACEMENT_ID1);
        Button ecpmSmallBanner =  findViewById(R.id.ecpmSmall) ;
        ecpmSmallBanner.setOnClickListener(this);
        Button customSmallBanner =  findViewById(R.id.customSmall);
        customSmallBanner.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        SharedPreferences spSetters;
        switch (v.getId()){
            case (R.id.buttonLoadAd):
//if(bannerSmall_request != null)
                BannerAdRequest bannerSmall_request = new BannerAdRequest();
                bannerViewSmall.setParams("_arFlag", "true");
                bannerViewSmall.setLanguage("KO");
                callBacks(bannerViewSmall);
                bannerViewSmall.loadAd(bannerSmall_request);
                break;

            case (R.id.customSmall):
                spSetters = getSharedPreferences("setters", MODE_PRIVATE);
                spSetters.edit().putString("whoAmI", "small").apply();
                startActivity(new Intent(SmallBanner.this, BannersConfig.class));
                finish();
                break;

            case (R.id.ecpmSmall):
                bannerViewSmall.getECPM( new BannerAdRequest(), new OnECPMLoaded() {
                    @Override
                    public void ecpm(ECPM ecpm) {
                        banner_requestSmall = new BannerAdRequest();
                        bannerViewSmall.loadAd(banner_requestSmall);
                        Log.v("ecpm", "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr() + ", banner ID: " + ecpm.getBanner());
                        Toast.makeText(SmallBanner.this, "ecpm: " + ecpm.getEcpm() + ", ppr: " + ecpm.getPpr() + ", banner ID: " + ecpm.getBanner(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void error(String s) {
                        Log.v("ecpm", "error: " + s);
                        Toast.makeText(SmallBanner.this, "Error ecpm", Toast.LENGTH_SHORT).show();
                        //ecpmData.setText("error: " + s);
                    }
                });
                break;
        }
    }

    private void callBacks(BannerView bannerViewSmall){
        bannerViewSmall.setBannerListener(new BannerListener(){
            @Override
            public void onError(AppnextError s) {
                super.onError(s);
                Log.e("==== crash --", s.getErrorMessage());
                Toast.makeText(SmallBanner.this, "Error " + s.getErrorMessage(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onAdLoaded(String adLoadedString) {
                super.onAdLoaded(adLoadedString);
                Log.e("==== Loaded -- ", adLoadedString);
                Toast.makeText(SmallBanner.this, "Loaded " + adLoadedString, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Log.e("==== Ad Clicked -- ", "Clicked");
                Toast.makeText(SmallBanner.this, "Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void adImpression() {
                super.adImpression();
                Log.e("==== Ad Impression -- ", "Impressed");
                Toast.makeText(SmallBanner.this, "Impressed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bannerViewSmall != null) {
            bannerViewSmall.destroy();
        }
    }

//    @Override
//    public void onStop(){
//        super.onStop();
//        if(bannerViewSmall != null) {
//            bannerViewSmall.destroy();
//        }
//    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
//        startActivity(new Intent(SmallBanner.this, BannerUnits.class));
//        finish();
    }
}
