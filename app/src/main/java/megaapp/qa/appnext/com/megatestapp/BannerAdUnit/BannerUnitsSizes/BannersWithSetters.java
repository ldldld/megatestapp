package megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerListener;
import com.appnext.banners.BannerView;
import com.appnext.core.AppnextError;

import megaapp.qa.appnext.com.megatestapp.banneradunit.BannerUnits;
import megaapp.qa.appnext.com.megatestapp.configuration.BannersConfig;
import megaapp.qa.appnext.com.megatestapp.R;

public class BannersWithSetters extends AppCompatActivity implements View.OnClickListener{

    private SharedPreferences spSetters;

    private BannerView bannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_with_setters);

        Button settersBtn = findViewById(R.id.settersBtn);
        settersBtn.setOnClickListener(this);

//        showDefaultBtnAll = (Button) findViewById(R.id.showDefaultBtnAll);
//        showDefaultBtnAll.setOnClickListener(this);

        Button showCustomBtnAll = findViewById(R.id.showCustomBtnAll);
        showCustomBtnAll.setOnClickListener(this);

        spSetters = getSharedPreferences("setters", MODE_PRIVATE);

        showCustomBtnAll.setEnabled(false);

        String callingActivity = spSetters.getString("whoAmI", "small");
        switch (callingActivity){
            case("small"):
                // showDefaultBtnAll.setText("Show Default Small");
                showCustomBtnAll.setText("Show Custom Small");
                break;

            case("medium"):
                // showDefaultBtnAll.setText("Show Default Medium");
                showCustomBtnAll.setText("Show Custom Medium");
                break;

            case("large"):
                // showDefaultBtnAll.setText("Show Default Large");
                showCustomBtnAll.setText("Show Custom Large");
                break;
        }
        Intent in = getIntent();
        if (in.getStringExtra("calling-activity") != null) {
            showCustomBtnAll.setEnabled(true);
        }
     //   Appnext.init(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.settersBtn):
                //startActivity(new Intent(AllBannersActivity.this, SettersActivity.class).putExtra("calling-activity", getIntent().getStringExtra("bannerType")));
//                startActivity(new Intent(AllBannersActivity.this, SettersActivity.class)
//                        .putExtra("calling-activity", getIntent().getStringExtra("bannerType"))
//                        .putExtra("bannerCallingSetters", "allBanners"));
                startActivity(new Intent(BannersWithSetters.this, BannersConfig.class));
                finish();
                break;

            case(R.id.showCustomBtnAll):
                //BannerView customBanner = new BannerView(this);
                //customBanner.setPlacementId(PLACEMENT_ID);
                //customBanner.setBannerSize(BannerSize.BANNER);
                BannerAdRequest banner_requestCustom = new BannerAdRequest();
                switch (spSetters.getString("creativeType", BannerAdRequest.TYPE_ALL)){

                    case ("All"):
                        banner_requestCustom.setCreativeType(BannerAdRequest.TYPE_ALL);
                        break;

                    case ("Static"):
                        banner_requestCustom.setCreativeType(BannerAdRequest.TYPE_STATIC);
                        break;

                    case ("Video"):
                        banner_requestCustom.setCreativeType(BannerAdRequest.TYPE_VIDEO);
                        break;
                }
                banner_requestCustom
                        .setCategories(spSetters.getString("category", "Games"))
                        .setPostback(spSetters.getString("postBack", "App Download"))
                        .setAutoPlay(spSetters.getBoolean("autoPlay", true))
                        .setMute(spSetters.getBoolean("mute", true))
                        .setClickEnabled(spSetters.getBoolean("clickenabled", false))
                        .setVideoLength(spSetters.getString("videoLength", "15"))
                        //.setVideoLength("LONG")
                        .setVidMin(spSetters.getInt("videoMin", 5))
                        .setVidMax(spSetters.getInt("videoMax", 10));
                if ((spSetters.getString("whoAmI", "small")).equals("small")){
                    //customBanner.setBannerSize(BannerSize.BANNER);
                    bannerView = findViewById(R.id.smallBanner);
                    bannerView.loadAd(banner_requestCustom);
                }else{
                    if ((spSetters.getString("whoAmI", "small")).equals("medium")){
                        // customBanner.setBannerSize(BannerSize.MEDIUM_RECTANGLE);
                        bannerView = findViewById(R.id.mediumBanner);
                        bannerView.loadAd(banner_requestCustom);
                    }else{
                        if ((spSetters.getString("whoAmI", "small")).equals("large")){
                            // customBanner.setBannerSize(BannerSize.LARGE_BANNER);
                            bannerView = findViewById(R.id.largeBanner);
                            bannerView.loadAd(banner_requestCustom);
                        }
                    }
                }
                callBacks();
                break;
        }
    }

    public void callBacks(){
        bannerView.setBannerListener(new BannerListener() {
            @Override
            public void onError(AppnextError s) {
                super.onError(s);
                Log.v("==== Error ", s.getErrorMessage());
                Toast.makeText(BannersWithSetters.this, "Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded(String s) {
                super.onAdLoaded(s);
                Toast.makeText(BannersWithSetters.this, "Ad Loaded " + s, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void adImpression() {
                super.adImpression();
                Toast.makeText(BannersWithSetters.this, "Ad Impression", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Toast.makeText(BannersWithSetters.this, "Ad Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bannerView != null) {
            bannerView.destroy();
        }
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        if(bannerView != null) {
            bannerView.destroy();
        }
        startActivity(new Intent(BannersWithSetters.this, BannerUnits.class));
        finish();
    }
}
