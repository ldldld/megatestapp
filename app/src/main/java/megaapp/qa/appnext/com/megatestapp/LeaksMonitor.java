package megaapp.qa.appnext.com.megatestapp;

import android.app.Application;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.squareup.leakcanary.LeakCanary;


public class LeaksMonitor extends Application {
    @Override public void onCreate() {
        super.onCreate();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // only for gingerbread and newer versions





            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (LeakCanary.isInAnalyzerProcess(LeaksMonitor.this)) {
                        Log.i("Running Leaks - ", "Leak Canary Running");
                        return;
                    }
                    LeakCanary.install(LeaksMonitor.this);
                }
            }, 2000);
        }
    }
}
