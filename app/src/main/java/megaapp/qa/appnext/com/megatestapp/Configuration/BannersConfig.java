package megaapp.qa.appnext.com.megatestapp.configuration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes.BannersWithSetters;
import megaapp.qa.appnext.com.megatestapp.R;

public class BannersConfig extends AppCompatActivity implements View.OnClickListener {
    private EditText postBackText;
    private EditText categories;
    private EditText videoLength;
    private EditText videoMinLength;
    private EditText videoMaxLength;


    private Spinner creativeTypeSpinner;

    private Switch autoPlaySwitch;
    private Switch muteSwitch;
    private Switch clickEnabledSwitch;

    private SharedPreferences spSetters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_config);

        spSetters = getSharedPreferences("setters", MODE_PRIVATE);

        String callingActivity = spSetters.getString("whoAmI", "small");
        // String callingActivity = getIntent().getStringExtra("bannerType");

        Button okBtn = findViewById(R.id.okBtn);
        okBtn.setOnClickListener(this);
        clickEnabledSwitch = findViewById(R.id.clickEnabledSwitch);
        clickEnabledSwitch.setChecked(false);
        postBackText = findViewById(R.id.postBackText);
        categories = findViewById(R.id.categoriesText);

        videoLength = findViewById(R.id.videoLengthET);
        videoMinLength = findViewById(R.id.minVideoLengthET);
        videoMaxLength = findViewById(R.id.maxVideoLengthET);

        muteSwitch = findViewById(R.id.muteSwitch);
        muteSwitch.setChecked(true);
        muteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        clickEnabledSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        autoPlaySwitch = findViewById(R.id.autoPlaySwitch);
        autoPlaySwitch.setChecked(false);
        autoPlaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            boolean selectionB;
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectionB = autoPlaySwitch.isChecked();
                // Toast.makeText(CustomEventInterstitial.this, String.valueOf(selectionB), Toast.LENGTH_SHORT).show();
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        creativeTypeSpinner = findViewById(R.id.creativeTypeSpinner);
        creativeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String creativeTypeSelected = parent.getItemAtPosition(position).toString();
                //Toast.makeText(CustomEventInterstitial.this, orientationSelected, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        defineOrigin(callingActivity);
    }

    public void defineOrigin(String callingActivity){

        if(callingActivity.equals("small") || callingActivity.equals("large")){
            muteSwitch.setEnabled(false);
            autoPlaySwitch.setEnabled(false);
            creativeTypeSpinner.setEnabled(false);
            videoLength.setEnabled(false);
            videoMaxLength.setEnabled(false);
            videoMinLength.setEnabled(false);
        }
    }

    public void settersPerBanner(String callingActivity) {

        //spSetters = getSharedPreferences("setters", MODE_PRIVATE);
        String creativeTypeSelected = creativeTypeSpinner.getSelectedItem().toString();
        spSetters.edit()
                .putString("category", categories.getText().toString())
                .putString("postBack", postBackText.getText().toString())
                .putString("videoLength", videoLength.getText().toString()).apply();
        if (videoMinLength.getText().toString().isEmpty()){
            spSetters.edit().putInt("videoMin", 5).apply();
        }else{
            spSetters.edit().putInt("videoMin", Integer.parseInt(videoMinLength.getText().toString())).apply();
        }

        if (videoMaxLength.getText().toString().isEmpty()){
            spSetters.edit().putInt("videoMax", 20).apply();
        }else{
            spSetters.edit().putInt("videoMax", Integer.parseInt(videoMaxLength.getText().toString())).apply();
        }

        spSetters.edit().putBoolean("mute", muteSwitch.isChecked())
                .putBoolean("autoPlay", autoPlaySwitch.isChecked())
                .putString("creativeType", creativeTypeSelected)
                .putBoolean("clickenabled", clickEnabledSwitch.isChecked())
                .apply();
    }

    @Override
    public void onClick(View view) {
        //spSetters = getSharedPreferences("setters", MODE_PRIVATE);
        settersPerBanner(spSetters.getString("whoAmI", "small"));
        startActivity(new Intent(BannersConfig.this, BannersWithSetters.class)
                .putExtra("showBtn", true)
                .putExtra("calling-activity", spSetters.getString("whoAmI", "small")));
        finish();
    }

    @Override
    public void onBackPressed(){
        Toast.makeText(this, "Click 'Save' Button", Toast.LENGTH_SHORT).show();
    }
}
