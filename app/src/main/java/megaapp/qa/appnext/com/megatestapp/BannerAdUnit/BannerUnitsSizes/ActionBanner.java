//package megaapp.qa.appnext.com.megatestapp.BannerAdUnit.BannerUnitsSizes;
//
//import android.content.Intent;
//import android.content.res.Configuration;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.appnext.actions.banner.AppBanner;
//import com.appnext.actions.banner.ServiceBanner;
//import com.appnext.banners.BannerAdRequest;
//import com.appnext.banners.BannerListener;
//import com.appnext.base.Appnext;
//import com.appnext.core.AppnextError;
//import megaapp.qa.appnext.com.megatestapp.BannerAdUnit.BannerUnits;
//import megaapp.qa.appnext.com.megatestapp.R;
//
//
//public class ActionBanner extends AppCompatActivity {
//    private AppBanner bannerSmall, bannerLarge;
//    private ServiceBanner bannerServiceSmall, bannerServiceLarge;
//    private  BannerAdRequest bannerRequest;
//    private static final String STATE_COUNTER = "counter";
//    private int mCounter;
//
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_action_banner3);
//        Appnext.init(this);
//        mCounter = 1;
//        bannerSmall = findViewById(R.id.bannerAppSmall);
//        bannerLarge = findViewById(R.id.bannerAppLarge);
//
//        bannerServiceSmall = findViewById(R.id.bannerServiceSmall);
//        bannerServiceLarge = findViewById(R.id.bannerServiceLarge);
//
//        bannerRequest = new BannerAdRequest();
//
//        Button loadActionBannerSmall = findViewById(R.id.loadActionBanner);
//        loadActionBannerSmall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadBanners("small", bannerSmall);
//            }
//        });
//
//        Button loadActionBannerLarge = findViewById(R.id.loadActionBannerB);
//        loadActionBannerLarge.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadBanners("large", bannerLarge);
//            }
//        });
//
//        Button loadServiceSmall = findViewById(R.id.loadServiceBanner);
//        loadServiceSmall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadServiceBanners("serviceSmall", bannerServiceSmall);
//            }
//        });
//
//        Button loadServiceLarge = findViewById(R.id.loadServiceBannerB);
//        loadServiceLarge.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loadServiceBanners("serviceLarge", bannerServiceLarge);
//            }
//        });
//    }
//
//    private void loadBanners(String bannerSize, AppBanner actionBannerType){
//
//        bannerRequest.setPostback(bannerSize + " action banner");
////        if (actionBannerType != null){
////            actionBannerType.destroy();
////        }
//        actionBannerType.loadAd(bannerRequest);
//        callBacks(actionBannerType);
//    }
//
//    private void loadServiceBanners(String serviceBannerSize, ServiceBanner serviceBanner){
//        bannerRequest.setPostback(serviceBannerSize + " action banner");
////        if (actionBannerType != null){
////            actionBannerType.destroy();
////        }
//        serviceBanner.loadAd(bannerRequest);
//        serviceCallBacks(serviceBanner);
//    }
//
//    private void callBacks(AppBanner actionBannerEvent){
//        actionBannerEvent.setBannerListener(new BannerListener(){
//            @Override
//            public void onError(AppnextError s) {
//                super.onError(s);
//                Log.e("==== crash --", s.getErrorMessage());
//                Toast.makeText(ActionBanner.this, "Error " + s.getErrorMessage(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdLoaded(String s) {
//                super.onAdLoaded(s);
//                Toast.makeText(ActionBanner.this, "Loaded " + s, Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void adImpression() {
//                super.adImpression();
//                Toast.makeText(ActionBanner.this, "Impressed", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdClicked() {
//                super.onAdClicked();
//                Toast.makeText(ActionBanner.this, "Clicked", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void serviceCallBacks(ServiceBanner serviceBanner){
//        serviceBanner.setBannerListener(new BannerListener(){
//            @Override
//            public void onError(AppnextError s) {
//                super.onError(s);
//                Log.e("==== crash --", s.getErrorMessage());
//                Toast.makeText(ActionBanner.this, "Error " + s.getErrorMessage(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdLoaded(String s) {
//                super.onAdLoaded(s);
//                Toast.makeText(ActionBanner.this, "Loaded " + s, Toast.LENGTH_SHORT).show();
//
//            }
//
//            @Override
//            public void adImpression() {
//                super.adImpression();
//                Toast.makeText(ActionBanner.this, "Impressed", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAdClicked() {
//                super.onAdClicked();
//                Toast.makeText(ActionBanner.this, "Clicked", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    @Override
//    public void onBackPressed(){
//        super.onBackPressed();
//        if(bannerSmall != null) {
//            bannerSmall.destroy();
//        }
//        if(bannerLarge != null) {
//            bannerLarge.destroy();
//        }
//        if(bannerServiceSmall != null) {
//            bannerServiceSmall.destroy();
//        }
//        if(bannerServiceLarge != null) {
//            bannerServiceLarge.destroy();
//        }
//        startActivity(new Intent(ActionBanner.this, BannerUnits.class));
//        finish();
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//
//        // Checks the orientation of the screen
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
//            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        // Make sure to call the super method so that the states of our views are saved
//        super.onSaveInstanceState(outState);
//        // Save our own state now
//        outState.putInt(STATE_COUNTER, mCounter);
//    }
//}
