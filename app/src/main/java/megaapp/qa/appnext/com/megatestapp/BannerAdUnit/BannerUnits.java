package megaapp.qa.appnext.com.megatestapp.banneradunit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//import megaapp.qa.appnext.com.adapters.AdMob.AdMobMainActivity;
//import megaapp.qa.appnext.com.megatestapp.BannerAdUnit.BannerUnitsSizes.ActionBanner;
import megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes.LargeBanner;
import megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes.MediumBanner;
import megaapp.qa.appnext.com.megatestapp.banneradunit.bannerunitssizes.SmallBanner;
import megaapp.qa.appnext.com.megatestapp.MainActivity;
import megaapp.qa.appnext.com.megatestapp.R;

public class BannerUnits extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_units);

        Button smallBannerUnitBtn = findViewById(R.id.smallBannerUnit);
        smallBannerUnitBtn.setOnClickListener(this);
        Button mediumBannerUnitBtn = findViewById(R.id.mediumBannerUnit);
        mediumBannerUnitBtn.setOnClickListener(this);
        Button largeBannerUnitBtn = findViewById(R.id.largeBannerUnit);
        largeBannerUnitBtn.setOnClickListener(this);
        Button actionBannerBtn = findViewById(R.id.actionBannerUnit);
        actionBannerBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.smallBannerUnit):
                startActivity(new Intent(BannerUnits.this, SmallBanner.class));
                //finish();
                break;

            case (R.id.mediumBannerUnit):
                startActivity(new Intent(BannerUnits.this, MediumBanner.class));
                //finish();
                break;

            case (R.id.largeBannerUnit):
                startActivity(new Intent(BannerUnits.this, LargeBanner.class));
                //finish();
                break;
//            case (R.id.actionBannerUnit):
//                startActivity(new Intent(BannerUnits.this, ActionBanner.class));
//                finish();
//                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(BannerUnits.this, MainActivity.class));
        finish();
    }
}
