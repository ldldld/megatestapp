package megaapp.qa.appnext.com.megatestapp.configuration;

import android.content.Context;

import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;

import java.util.HashMap;

public class InterstitialSettersCreator {
    private static Interstitial interstitial;
    private static HashMap<String, Object> interstitialHash;

    public static Interstitial interstitialSettersCreator(final String PID, final Context context) {

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
                if(interstitial == null){
                    interstitial = new Interstitial(context, PID);
                }


                interstitialHash = new HashMap<>();
                interstitialHash.put("interstitial",interstitial);

//            }
//        }).run();

        return interstitial;
    }
}
