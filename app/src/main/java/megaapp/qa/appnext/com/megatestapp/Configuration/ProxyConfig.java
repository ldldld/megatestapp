//package megaapp.qa.appnext.com.megatestapp.Configuration;
//
//
//import android.annotation.TargetApi;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.location.Criteria;
//import android.location.Location;
//import android.location.LocationManager;
//import android.net.ProxyInfo;
//import android.net.wifi.WifiConfiguration;
//import android.net.wifi.WifiEnterpriseConfig;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.os.Build;
//import android.os.Handler;
//import android.os.IBinder;
//import android.os.SystemClock;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.widget.Toast;
//
//import java.lang.reflect.Constructor;
//import java.lang.reflect.Field;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.net.InetAddress;
//import java.util.ArrayList;
//import java.util.List;
//
//public class ProxyConfig extends Service{
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        setLocation(getApplicationContext());
//        setWifiProxySettings5(getApplicationContext());
//    }
//
////public static void setWifiProxySettings5(Context context) {
//
////        WifiConfiguration wifiConf = null;
////        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
////        WifiInfo connectionInfo = null;
////        if (wifiManager != null) {
////            connectionInfo = wifiManager.getConnectionInfo();
////        }
////        List<WifiConfiguration> configuredNetworks = null;
////        if (wifiManager != null) {
////            configuredNetworks = wifiManager.getConfiguredNetworks();
////        }
////        if (configuredNetworks != null) {
////            for (WifiConfiguration conf : configuredNetworks) {
////                if (conf.networkId == connectionInfo.getNetworkId()) {
////                    wifiConf = conf;
////                    break;
////                }
////            }
////        }
////
////        try{
////            setIpAssignment("STATIC", wifiConf); //or "DHCP" for dynamic setting
////            setIpAddress(InetAddress.getByName("148.251.100.118"), 24, wifiConf);
////            setGateway(InetAddress.getByName("148.251.100.118"), wifiConf);
////            setDNS(InetAddress.getByName("148.251.100.118"), wifiConf);
////            wifiManager.updateNetwork(wifiConf); //apply the setting
////            wifiManager.saveConfiguration(); //Save it
////        }catch(Exception e){
////            e.printStackTrace();
////        }
////    }
////
////    public static void setIpAssignment(String assign , WifiConfiguration wifiConf)
////            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
////        setEnumField(wifiConf, assign, "ipAssignment");
////    }
////
////    public static void setIpAddress(InetAddress addr, int prefixLength, WifiConfiguration wifiConf)
////            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
////            NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException{
////        Object linkProperties = getField(wifiConf, "linkProperties");
////        if(linkProperties == null)return;
////        Class laClass = Class.forName("android.net.LinkAddress");
////        Constructor laConstructor = laClass.getConstructor(new Class[]{InetAddress.class, int.class});
////        Object linkAddress = laConstructor.newInstance(addr, prefixLength);
////
////        ArrayList mLinkAddresses = (ArrayList)getDeclaredField(linkProperties, "mLinkAddresses");
////        mLinkAddresses.clear();
////        mLinkAddresses.add(linkAddress);
////    }
////
////    public static void setGateway(InetAddress gateway, WifiConfiguration wifiConf)
////            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
////            ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException{
////        Object linkProperties = getField(wifiConf, "linkProperties");
////        if(linkProperties == null)return;
////        Class routeInfoClass = Class.forName("android.net.RouteInfo");
////        Constructor routeInfoConstructor = routeInfoClass.getConstructor(new Class[]{InetAddress.class});
////        Object routeInfo = routeInfoConstructor.newInstance(gateway);
////
////        ArrayList mRoutes = (ArrayList)getDeclaredField(linkProperties, "mRoutes");
////        mRoutes.clear();
////        mRoutes.add(routeInfo);
////    }
////
////    public static void setDNS(InetAddress dns, WifiConfiguration wifiConf)
////            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
////        Object linkProperties = getField(wifiConf, "linkProperties");
////        if(linkProperties == null)return;
////
////        ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>)getDeclaredField(linkProperties, "mDnses");
////        mDnses.clear(); //or add a new dns address , here I just want to replace DNS1
////        mDnses.add(dns);
////    }
////
////    public static Object getField(Object obj, String name)
////            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
////        Field f = obj.getClass().getField(name);
////        Object out = f.get(obj);
////        return out;
////    }
////
////    public static Object getDeclaredField(Object obj, String name)
////            throws SecurityException, NoSuchFieldException,
////            IllegalArgumentException, IllegalAccessException {
////        Field f = obj.getClass().getDeclaredField(name);
////        f.setAccessible(true);
////        Object out = f.get(obj);
////        return out;
////    }
////
////    private static void setEnumField(Object obj, String value, String name)
////            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
////        Field f = obj.getClass().getField(name);
////        f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
////    }
//
////    public static void setGateway(InetAddress gateway, WifiConfiguration wifiConf)
////            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
////            ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException{
////        Object linkProperties = getField(wifiConf, "linkProperties");
////        if(linkProperties == null)return;
////        ArrayList mGateways = (ArrayList)getDeclaredField(linkProperties, "mGateways");
////        mGateways.clear();
////        mGateways.add(gateway);
////    }
//
//    public static void setLocation(Context context){
//        LocationManager lm = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        criteria.setAccuracy( Criteria.ACCURACY_FINE );
//
//        String mocLocationProvider = LocationManager.GPS_PROVIDER;//lm.getBestProvider( criteria, true );
//
//        if ( mocLocationProvider == null ) {
//            Toast.makeText(context.getApplicationContext(), "No location provider found!", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if (lm != null) {
//            lm.addTestProvider(mocLocationProvider, false, false,
//                    false, false, true, true, true, 0, 5);
//        }
//        if (lm != null) {
//            lm.setTestProviderEnabled(mocLocationProvider, true);
//        }
//
//        Location loc = new Location(mocLocationProvider);
//        Location mockLocation = new Location(mocLocationProvider); // a string
//        mockLocation.setLatitude(52.5318504);  // double
//        mockLocation.setLongitude(13.3634694);
//        mockLocation.setAltitude(loc.getAltitude());
//        mockLocation.setTime(System.currentTimeMillis());
//        mockLocation.setAccuracy(1);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
//        }
//        if (lm != null) {
//            lm.setTestProviderLocation( mocLocationProvider, mockLocation);
//        }
//        Toast.makeText(context.getApplicationContext(), "Working", Toast.LENGTH_SHORT).show();
//    }
//        static WifiConfiguration GetCurrentWifiConfiguration (WifiManager manager)
//        {
//            if (!manager.isWifiEnabled())
//                return null;
//
//            List<WifiConfiguration> configurationList = manager.getConfiguredNetworks();
//            WifiConfiguration configuration = null;
//            int cur = manager.getConnectionInfo().getNetworkId();
//            for (int i = 0; i < configurationList.size(); ++i) {
//                WifiConfiguration wifiConfiguration = configurationList.get(i);
//                if (wifiConfiguration.networkId == cur)
//                    configuration = wifiConfiguration;
//            }
//
//            return configuration;
//        }
//
//
//        @TargetApi(Build.VERSION_CODES.M)
//        public static void setWifiProxySettings5 (Context context)
//        {
//            //get the current wifi configuration
//            WifiManager manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//            WifiConfiguration config = GetCurrentWifiConfiguration(manager);
//            if (null == config)
//                return;
//
//            try {
//                //linkProperties is no longer in WifiConfiguration
//                Class proxyInfoClass = Class.forName("android.net.ProxyInfo");
//                Class[] setHttpProxyParams = new Class[1];
//                setHttpProxyParams[0] = proxyInfoClass;
//                Class wifiConfigClass = Class.forName("android.net.wifi.WifiConfiguration");
//                Method setHttpProxy = wifiConfigClass.getDeclaredMethod("setHttpProxy", setHttpProxyParams);
//                setHttpProxy.setAccessible(true);
//
//                //Method 1 to get the ENUM ProxySettings in IpConfiguration
//                Class ipConfigClass = Class.forName("android.net.IpConfiguration");
//                Field f = ipConfigClass.getField("proxySettings");
//                Class proxySettingsClass = f.getType();
//
//                //Method 2 to get the ENUM ProxySettings in IpConfiguration
//                //Note the $ between the class and ENUM
//                //Class proxySettingsClass = Class.forName("android.net.IpConfiguration$ProxySettings");
//
//                Class[] setProxySettingsParams = new Class[1];
//                setProxySettingsParams[0] = proxySettingsClass;
//                Method setProxySettings = wifiConfigClass.getDeclaredMethod("setProxySettings", setProxySettingsParams);
//                setProxySettings.setAccessible(true);
//
//
//                ProxyInfo pi = ProxyInfo.buildDirectProxy("144.76.7.248", 8080);
//                //Android 5 supports a PAC file
//                //ENUM value is "PAC"
//                //ProxyInfo pacInfo = ProxyInfo.buildPacProxy(Uri.parse("http://localhost/pac"));
//
//                //pass the new object to setHttpProxy
//                Object[] params_SetHttpProxy = new Object[1];
//                params_SetHttpProxy[0] = pi;
//                setHttpProxy.invoke(config, params_SetHttpProxy);
//
//                //pass the enum to setProxySettings
//                Object[] params_setProxySettings = new Object[1];
//                params_setProxySettings[0] = Enum.valueOf((Class<Enum>) proxySettingsClass, "STATIC");
//                setProxySettings.invoke(config, params_setProxySettings);
//
//                //save the settings
//                manager.updateNetwork(config);
//                manager.saveConfiguration();
//                manager.disconnect();
//                manager.reconnect();
//            } catch (Exception e) {
//                Log.v("wifiProxy", e.toString());
//            }
//        }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//
//        return null;
//    }
//    // }
//}
