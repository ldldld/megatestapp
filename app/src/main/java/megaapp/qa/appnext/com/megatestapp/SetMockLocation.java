//package megaapp.qa.appnext.com.megatestapp;
//
//import android.content.Context;
//import android.location.Criteria;
//import android.location.Location;
//import android.location.LocationManager;
//import android.os.Build;
//import android.os.SystemClock;
//import android.widget.Toast;
//
//class SetMockLocation {
//    public static void setLocation(Context context){
//        LocationManager lm = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        criteria.setAccuracy( Criteria.ACCURACY_FINE );
//
//        String mocLocationProvider = LocationManager.GPS_PROVIDER;//lm.getBestProvider( criteria, true );
//
//        if ( mocLocationProvider == null ) {
//            Toast.makeText(context.getApplicationContext(), "No location provider found!", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if (lm != null) {
//            lm.addTestProvider(mocLocationProvider, false, false,
//                    false, false, true, true, true, 0, 5);
//        }
//        if (lm != null) {
//            lm.setTestProviderEnabled(mocLocationProvider, true);
//        }
//
//        Location loc = new Location(mocLocationProvider);
//        Location mockLocation = new Location(mocLocationProvider); // a string
//        mockLocation.setLatitude(52.5318504);  // double
//        mockLocation.setLongitude(13.3634694);
//        mockLocation.setAltitude(loc.getAltitude());
//        mockLocation.setTime(System.currentTimeMillis());
//        mockLocation.setAccuracy(1);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
//        }
//        if (lm != null) {
//            lm.setTestProviderLocation( mocLocationProvider, mockLocation);
//        }
//        Toast.makeText(context.getApplicationContext(), "Working", Toast.LENGTH_SHORT).show();
//    }
//}
