package megaapp.qa.appnext.com.megatestapp.nativeadssdknew;

import android.app.Application;
import android.os.Build;

//import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;

//import io.fabric.sdk.android.Fabric;


public class MyApp extends Application {
    @Override public void onCreate() {
        super.onCreate();
       // Fabric.with(this, new Crashlytics());
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            if (LeakCanary.isInAnalyzerProcess(this)) {
                return;
            }
            LeakCanary.install(this);
        }
    }
}

