package megaapp.qa.appnext.com.megatestapp.androidsdk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.appnext.ads.interstitial.Interstitial;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
import megaapp.qa.appnext.com.megatestapp.R;

public class MultiProccesActivity extends AppCompatActivity {
private  Interstitial interstitial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_procces);

        interstitial = new Interstitial(this, "b308acb6-462b-42f0-8ef3-6ba46aba7ef0");
        interstitial.setLanguage("KO");
        callBacks();
        interstitial.loadAd();
    }

    private void callBacks(){
        interstitial.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String s) {

            }
        });

        interstitial.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {

            }
        });

        interstitial.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                interstitial.destroy();
                interstitial = null;
                startActivity(new Intent(MultiProccesActivity.this, AndroidSdkMainActivity.class));
                finish();
            }
        });

        interstitial.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s) {
                interstitial.showAd();
            }
        });

        interstitial.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {

            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MultiProccesActivity.this, AndroidSdkMainActivity.class));
        finish();
    }
}
