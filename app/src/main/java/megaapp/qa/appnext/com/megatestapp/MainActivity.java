package megaapp.qa.appnext.com.megatestapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.security.NetworkSecurityPolicy;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.appnext.base.Appnext;
import megaapp.qa.appnext.com.megatestapp.actionssdk.ActionsSdkMainActivity;
import megaapp.qa.appnext.com.megatestapp.androidsdk.AndroidSdkMainActivity;
import megaapp.qa.appnext.com.megatestapp.banneradunit.BannerUnits;
import megaapp.qa.appnext.com.megatestapp.nativeadssdk.NativeAdsActivity;
import megaapp.qa.appnext.com.megatestapp.nativeadssdknew.NativeAdsActivityNew;
import megatestapp.qa.appnext.com.adapters.admob.AdMobMainActivity;
import megatestapp.qa.appnext.com.adapters.mopub.MoPubMainActivity;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new LeaksMonitor();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
        }
//        LoginDatabaseAdapter loginDataBaseAdapter;
//        loginDataBaseAdapter=new LoginDatabaseAdapter(getApplicationContext());
//        loginDataBaseAdapter = loginDataBaseAdapter.open();
//        loginDataBaseAdapter.insertEntry("name", "lastName", "userName", "password");
//        loginDataBaseAdapter.close();
        Appnext.init(this);
//        startService(new Intent(MainActivity.this, ProxyConfig.class));
//        ProxyConfig.setLocation(this.getApplicationContext());
//        ProxyConfig.setWifiProxySettings5(this.getApplicationContext());
        Button nativeSDK = findViewById(R.id.nativeSDK);
        nativeSDK.setOnClickListener(this);

        Button nativeAdsNew = findViewById(R.id.nativeSDKnew);
        nativeAdsNew.setOnClickListener(this);

        Button moPubSdk = findViewById(R.id.moPubSDK);
        moPubSdk.setOnClickListener(this);

        Button adMobSDK = findViewById(R.id.adMobSDK);
        adMobSDK.setOnClickListener(this);

        Button actionsSDK = findViewById(R.id.actionsSDK);
        actionsSDK.setOnClickListener(this);

        Button bannerAdUnitSDK = findViewById(R.id.bannerAdUnitSDK);
        bannerAdUnitSDK.setOnClickListener(this);

        Button androidSDK = findViewById(R.id.androidSDK);
        androidSDK.setOnClickListener(this);

        Button clearSP = findViewById(R.id.clearSP);
        clearSP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences("lib_shared_preferences", MODE_PRIVATE);
                preferences.edit().clear().apply();
            }
        });
        //Appnext.setParam("consent","true");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case (R.id.nativeSDK):
                startActivity(new Intent(MainActivity.this, NativeAdsActivity.class));
                finish();
                break;

            case (R.id.nativeSDKnew):
                startActivity(new Intent(MainActivity.this, NativeAdsActivityNew.class));
                finish();
                break;

            case (R.id.moPubSDK):
                startActivity(new Intent(MainActivity.this, MoPubMainActivity.class));
                finish();
                break;

            case (R.id.adMobSDK):
                startActivity(new Intent(MainActivity.this, AdMobMainActivity.class));
                finish();
                break;

            case (R.id.actionsSDK):
                startActivity(new Intent(MainActivity.this, ActionsSdkMainActivity.class));
                finish();

                break;

            case (R.id.bannerAdUnitSDK):
                startActivity(new Intent(MainActivity.this, BannerUnits.class));
                finish();
                break;

            case (R.id.androidSDK):
                startActivity(new Intent(MainActivity.this, AndroidSdkMainActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
