package megaapp.qa.appnext.com.megatestapp.nativeadssdknew;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.appnext.core.AppnextError;
import com.appnext.nativeads.MediaView;
import com.appnext.nativeads.NativeAd;
import com.appnext.nativeads.NativeAdListener;
import com.appnext.nativeads.NativeAdRequest;
import com.appnext.nativeads.NativeAdView;
import com.appnext.nativeads.PrivacyIcon;
import java.util.ArrayList;
import java.util.List;
import megaapp.qa.appnext.com.megatestapp.configuration.NativeAdsSdkNewConfig;
import megaapp.qa.appnext.com.megatestapp.R;

public class MultiProccesNativeActivity extends AppCompatActivity {
    private final static String PID_2 = "4c8fd5c8-0ed1-411a-95fd-709d88d4fda7";

    private SharedPreferences spSetters;
    private NativeAdView nativeAdView2;
    private MediaView mediaView2;
    private Button installBtn2;
    private NativeAd nativeAd2 = null;
    private NativeAdRequest adRequest2;
    private TextView title2;
    private ImageView icon2;
//    private TextView bannerId;
//    private TextView adDescription;
//    private TextView imageURL;
//    private TextView imageURLWide;
//    private TextView urlVideo;
//    private TextView adPackage;
//    private TextView country;
//    private TextView supportedVersion;
//    private TextView storeRating;
//    private TextView storeDownloads;
//    private TextView appSize;
//    private TextView categories;
//    private TextView ecpmTv;
//    private TextView pprTv;
    private List<View> clickableViews2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_procces_native);


      //  Appnext.init(this);
        clickableViews2= new ArrayList<>();

        Button settersBtn = findViewById(R.id.settersBtn);
        Button loadSettersButton = findViewById(R.id.loadSettersButton);
        spSetters = getSharedPreferences("setters", MODE_PRIVATE);
        System.out.println("@#@# "+ spSetters.getAll().size());
        title2 = findViewById(R.id.na_title2);
        icon2 = findViewById(R.id.na_icon2);
        nativeAdView2 = findViewById(R.id.na_view2);
        mediaView2 = findViewById(R.id.na_media2);
        Button loadButton = findViewById(R.id.loadAdButton);
        Button ecpmAdButton = findViewById(R.id.ecpmAdButton);
        installBtn2 = findViewById(R.id.installBtn2);
        settersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiProccesNativeActivity.this, NativeAdsSdkNewConfig.class));
                finish();
            }
        });
        System.out.println("@#@# before disable if "+ spSetters.getAll().size());
        if(spSetters.getAll().isEmpty()){
            loadSettersButton.setEnabled(false);
            System.out.println("@#@# disabled");
        }else{
            loadSettersButton.setEnabled(true);
            System.out.println("@#@# enabled");
        }
        loadSettersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeInstances();
                // scrollView.pageScroll(View.FOCUS_UP);
                if (nativeAd2 != null){
                    nativeAd2.destroy();
                    nativeAdView2.destroyDrawingCache();
                    mediaView2.destroy();
                    clickableViews2.clear();

                }

                if (spSetters == null) {
                    Toast.makeText(MultiProccesNativeActivity.this, "Setters not defined", Toast.LENGTH_SHORT).show();
                } else {

                    adRequest2 = new NativeAdRequest();
                    nativeAd2 = new NativeAd(MultiProccesNativeActivity.this, PID_2);
                    nativeAd2.setPrivacyPolicyColor(spSetters.getInt("privacyColor", PrivacyIcon.PP_ICON_COLOR_LIGHT));
                    nativeAd2.setPrivacyPolicyPosition(spSetters.getInt("privacyPosition", PrivacyIcon.PP_ICON_POSITION_TOP_RIGHT));
                    adRequest2
                            .setPostback(spSetters.getString("postBack", "Successfully Installed"))
                            .setCategories(spSetters.getString("category", "Games"))
                            .setMinVideoLength(spSetters.getInt("videoMin", 5))
                            .setMaxVideoLength(spSetters.getInt("videoMax", 10));
                    mediaView2.setMute(spSetters.getBoolean("mute", true));
                    mediaView2.setAutoPLay(spSetters.getBoolean("autoPlay", true));
                    mediaView2.setClickEnabled(spSetters.getBoolean("clickEnabled", true));
                    switch (spSetters.getString("creativeType", "All")) {
                        case ("All"):
                            adRequest2.setCreativeType(NativeAdRequest.CreativeType.ALL);
                            break;
                        case ("Static"):
                            adRequest2.setCreativeType(NativeAdRequest.CreativeType.STATIC_ONLY);
                            break;
                        case ("Video"):
                            adRequest2.setCreativeType(NativeAdRequest.CreativeType.VIDEO_ONLY);
                            break;
                    }
                    switch (spSetters.getString("videoQuality", "Default")) {
                        case ("Default"):
                            adRequest2.setVideoQuality(NativeAdRequest.VideoQuality.DEFAULT);
                            break;
                        case ("Low"):
                            adRequest2.setVideoQuality(NativeAdRequest.VideoQuality.LOW);
                            break;
                        case ("High"):
                            adRequest2.setVideoQuality(NativeAdRequest.VideoQuality.HIGH);
                            break;
                    }
                    switch (spSetters.getString("videoLength", "Default")) {
                        case ("Default"):
                            adRequest2.setVideoLength(NativeAdRequest.VideoLength.DEFAULT);
                            break;
                        case ("Short"):
                            adRequest2.setVideoLength(NativeAdRequest.VideoLength.SHORT);
                            break;
                        case ("Long"):
                            adRequest2.setVideoLength(NativeAdRequest.VideoLength.LONG);
                            break;
                    }
                    switch (spSetters.getString("cachingPolicy", "All")) {
                        case ("All"):
                            adRequest2.setCachingPolicy(NativeAdRequest.CachingPolicy.ALL);
                            break;
                        case ("Nothing"):
                            adRequest2.setCachingPolicy(NativeAdRequest.CachingPolicy.NOTHING);
                            break;
                        case ("Video Only"):
                            adRequest2.setCachingPolicy(NativeAdRequest.CachingPolicy.VIDEO_ONLY);
                            break;
                        case ("Static Only"):
                            adRequest2.setCachingPolicy(NativeAdRequest.CachingPolicy.STATIC_ONLY);
                            break;
                    }

                    title2.setText(nativeAd2.getAdTitle());
                    nativeAd2.downloadAndDisplayImage(icon2, nativeAd2.getIconURL());
                    clickableViews2.add(title2);
                    clickableViews2.add(icon2);
                    clickableViews2.add(installBtn2);
                    clickableViews2.add(mediaView2);
                    nativeAd2.registerClickableViews(clickableViews2);
                    nativeAd2.loadAd(adRequest2);
                    callBacks();
                }
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeInstances();
                if (nativeAd2 != null){
                    nativeAd2.destroy();
                    nativeAdView2.destroyDrawingCache();
                    mediaView2.destroy();
                    clickableViews2.clear();
                }

                nativeAd2 = new NativeAd(MultiProccesNativeActivity.this, PID_2);
                adRequest2 = new NativeAdRequest();
                nativeAd2.loadAd(adRequest2
                        .setPostback("")
                        .setCategories("")
                        .setCachingPolicy(NativeAdRequest.CachingPolicy.ALL)
                        .setCreativeType(NativeAdRequest.CreativeType.ALL)
                        .setVideoLength(NativeAdRequest.VideoLength.SHORT)
                        .setVideoQuality(NativeAdRequest.VideoQuality.HIGH)
                        .setMinVideoLength(5)
                        .setMaxVideoLength(20)


                );
                mediaView2.setMute(false);
                mediaView2.setAutoPLay(true);
                mediaView2.setClickEnabled(false);
                callBacks();

            }
        });
        ecpmAdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nativeAd2.getECPM();
                nativeAd2.getPPR();
                //Categories - the current banner app categories (getCategories)
                Log.i("Categories - ", nativeAd2.getCategories());
                //App Package - the current banner app package name (getPackage)
                Log.i("Package Name - ", nativeAd2.getAppPackageName());
                //Supported Versions - the current app supported android versions (getSupportedVersion)
                Log.i("Supported Version - ", nativeAd2.getSupportedVersion() );
                // nativeAd2.destroy();
                Toast.makeText(MultiProccesNativeActivity.this, "ecpm : " + nativeAd2.getECPM(), Toast.LENGTH_SHORT).show();
                Toast.makeText(MultiProccesNativeActivity.this, "ppr : " + nativeAd2.getPPR(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void callBacks(){
        //   spSetters = getSharedPreferences("setters", MODE_PRIVATE);
        nativeAd2.setAdListener(new NativeAdListener() {
            @Override
            public void onAdLoaded(NativeAd nativeAd) {
                Toast.makeText(MultiProccesNativeActivity.this, "Loaded", Toast.LENGTH_SHORT).show();
                title2.setText(nativeAd2.getAdTitle());
                nativeAd2.downloadAndDisplayImage(icon2, nativeAd2.getIconURL());
                clickableViews2.add(title2);
                clickableViews2.add(icon2);
                clickableViews2.add(installBtn2);
                nativeAd2.registerClickableViews(clickableViews2);
                nativeAd2.setNativeAdView(nativeAdView2);
                nativeAd2.setMediaView(mediaView2);

                installBtn2.setText(nativeAd2.getCTAText());


            }

            @Override
            public void onAdClicked(NativeAd nativeAd) {
                super.onAdClicked(nativeAd);
            }


            @Override
            public void onError(NativeAd nativeAd, AppnextError appnextError) {

                super.onError(nativeAd, appnextError);
                Toast.makeText(MultiProccesNativeActivity.this, appnextError.getErrorMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void adImpression(NativeAd nativeAd) {
                super.adImpression(nativeAd);
                Toast.makeText(MultiProccesNativeActivity.this, "I'm impressed from " + nativeAd.getAdTitle(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeInstances();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        removeInstances();
        startActivity(new Intent(MultiProccesNativeActivity.this, NativeAdsActivityNew.class));
        finish();
    }

    public void removeInstances(){
//        if (nativeAd2 != null){
//            nativeAd2.destroy();
//        }
//        if(nativeAdView2 != null){
//            nativeAdView2.destroyDrawingCache();
//        }
//        if(mediaView2 != null){
//            mediaView2.destroy();
//        }

        if (nativeAd2 != null){
            nativeAd2.destroy();
            nativeAdView2.destroyDrawingCache();
            mediaView2.destroy();
            clickableViews2.clear();
        }
    }
}
