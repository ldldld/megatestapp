package megaapp.qa.appnext.com.megatestapp.actionssdk;

/**
 * Created by leonid on 12/11/2017.
 */

public class ActionCustomObject {
    private String name, param;
    private boolean isSelected;

    public ActionCustomObject(String data) {
        this.name = data.substring(0, data.indexOf(" ("));
        this.param = data.substring(data.indexOf("(") + 1, data.indexOf(")"));
        this.isSelected = false;
    }

    public String getName() {
        return name;
    }

    public String getParam() {
        return param;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
