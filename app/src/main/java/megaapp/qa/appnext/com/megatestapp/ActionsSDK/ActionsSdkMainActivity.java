package megaapp.qa.appnext.com.megatestapp.actionssdk;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.appnext.actionssdk.Action;
import com.appnext.actionssdk.ActionData;
import com.appnext.actionssdk.ActionError;
import com.appnext.actionssdk.ActionSDK;
import com.appnext.actionssdk.callback.OnActionClosed;
import com.appnext.actionssdk.callback.OnActionError;
import com.appnext.actionssdk.callback.OnActionOpened;
import com.appnext.actionssdk.callback.OnActionsLoaded;
import com.appnext.actionssdk.callback.OnAppClicked;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.appnextsdk.API.AppnextAPI;
import com.appnext.appnextsdk.API.AppnextAd;
import com.appnext.appnextsdk.API.AppnextAdRequest;
import com.appnext.core.callbacks.OnAdLoaded;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import megaapp.qa.appnext.com.megatestapp.actionssdk.actionsAdapter.ActionSelectAdapter;
import megaapp.qa.appnext.com.megatestapp.actionssdk.actionsAdapter.ItemListAdapter;
import megaapp.qa.appnext.com.megatestapp.MainActivity;
import megaapp.qa.appnext.com.megatestapp.R;

public class ActionsSdkMainActivity extends AppCompatActivity implements ActionSDK.MomentsCallback, OnActionsLoaded {

    private ActionSDK actionSDK;
    private ArrayList<ActionData> actions;
    private List<String> streamActions;
    private List<ActionCustomObject> actionsCustomObjectList;
    private ItemListAdapter actionsAdapter;
    private Switch streamActionsSwitch;
    private ProgressBar loader;
    private Interstitial interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actions_sdk_main);
        actionSDK = new ActionSDK(getApplicationContext(), "b7bd9d3a-368b-453e-b12c-961f0122f152");
        Button btnEN, btnRU, btnGR, btnCN;
        btnEN = findViewById(R.id.btnEN);
        testOtherAppnextSDKs();

//        btnEN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
                actionSDK.setLanguage("en");
//            }
//        });
//        btnRU = findViewById(R.id.btnRU);
//        btnRU.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
                actionSDK.setLanguage("ru");
//            }
//        });
//        btnGR = findViewById(R.id.btnGR);
//        btnGR.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
                actionSDK.setLanguage("de");
//            }
//        });
//        btnCN = findViewById(R.id.btnCN);
//        btnCN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
                actionSDK.setLanguage("zh");
//            }
//        });
        streamActions = new ArrayList<>();
        List<String> actionsList = Arrays.asList(getResources().getStringArray(R.array.actions_array));
        actionsCustomObjectList = new ArrayList<>();
        for (int i = 0; i < actionsList.size(); i++) {
            ActionCustomObject aco = new ActionCustomObject(actionsList.get(i));
            actionsCustomObjectList.add(aco);
        }
        Button allActionsButton = findViewById(R.id.all_actions);
        Button momentActionButton = findViewById(R.id.moment_actions);
        Button streamActionsButton = findViewById(R.id.stream_actions);
        Button crashButton = findViewById(R.id.crash);
        Button chooseActionsButton = findViewById(R.id.choose_actions);
        loader = findViewById(R.id.loader);
        Switch iconsColorSwitch = findViewById(R.id.icons_color);
        streamActionsSwitch = findViewById(R.id.stream_actions_switch);
        crashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int [] ronny = {1,2};
                Log.i("ronny", String.valueOf(ronny[2]));
            }
        });

        allActionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.setVisibility(View.VISIBLE);
                actionSDK.loadActions(ActionsSdkMainActivity.this);
            }
        });

        chooseActionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActionsSdkMainActivity.this);
                builder.setTitle("Choose Action");
                final LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View v = inflater.inflate(R.layout.alert_dialog_multi_list, null);
                builder.setView(v);
                final ListView listView = v.findViewById(R.id.multi_list);
                final ActionSelectAdapter adapter = new ActionSelectAdapter(getApplicationContext(), actionsCustomObjectList);
                listView.setAdapter(adapter);
                listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                listView.setFastScrollEnabled(false);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loader.setVisibility(View.VISIBLE);
                        ArrayList<String> selectedActionsParams = new ArrayList<>();
                        for (ActionCustomObject aco : actionsCustomObjectList) {
                            if (aco.isSelected()) {
                                selectedActionsParams.add(aco.getParam());
                            }
                        }
                        if (selectedActionsParams.size() > 0) {
                            String[] selectedActionParamArr = new String[selectedActionsParams.size()];
                            selectedActionParamArr = selectedActionsParams.toArray(selectedActionParamArr);
                            actionSDK.loadActions(ActionsSdkMainActivity.this, selectedActionParamArr);
                        } else {
                            if (actionsAdapter != null)
                                actionsAdapter.clear();
                            loader.setVisibility(View.GONE);
                        }
                    }
                });
                builder.show();
            }
        });

        momentActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.setVisibility(View.VISIBLE);
                if (actionsAdapter != null)
                    actionsAdapter.clear();
                actionSDK.loadMoments(ActionsSdkMainActivity.this);
            }
        });

        streamActionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.setVisibility(View.VISIBLE);
                if (actionsAdapter != null)
                    actionsAdapter.clear();
                if (streamActions.size() > 0) {
                    String[] streamActionArr = new String[streamActions.size()];
                    streamActionArr = streamActions.toArray(streamActionArr);
                    actionSDK.loadActions(ActionsSdkMainActivity.this, streamActionArr);
                } else {
                    loader.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "No Stream Actions", Toast.LENGTH_SHORT).show();
                }
            }
        });

        iconsColorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    actionSDK.setActionIconColor(Action.ACTION_COLOR_BLACK);
                } else {
                    actionSDK.setActionIconColor(Action.ACTION_COLOR_WHITE);
                }
            }
        });

        streamActionsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    actionSDK.startMomentCallback(ActionsSdkMainActivity.this);
                    Toast.makeText(getApplicationContext(), "Streaming On", Toast.LENGTH_SHORT).show();
                } else {
                    actionSDK.stopMomentCallback();
                    Toast.makeText(getApplicationContext(), "Streaming Off", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Callbacks
        actionSDK.setOnActionErrorCallback(new OnActionError() {
            @Override
            public void actionError(String action, String error) {
                loader.setVisibility(View.GONE);
                String errorString = "Error: ";

                if (error.equals(ActionError.ACTION_NOT_READY)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.INTERNAL_ERROR)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.NO_MARKET)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.CONNECTION_ERROR)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.NO_ADS)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.ACTION_EXPIRED)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.TIMEOUT)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.PACKAGE_NOT_APPROVED)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
                if (error.equals(ActionError.SLOW_CONNECTION)) {
                    Toast.makeText(getApplicationContext(), errorString + error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        actionSDK.setOnAppClickedCallback(new OnAppClicked() {
            @Override
            public void appClicked() {
                Toast.makeText(getApplicationContext(), "action clicked", Toast.LENGTH_SHORT).show();
            }
        });
//
        actionSDK.setOnActionClosedCallback(new OnActionClosed() {
            @Override
            public void actionClosed() {
                Toast.makeText(getApplicationContext(), "action closed", Toast.LENGTH_SHORT).show();
            }
        });

        actionSDK.setOnActionOpenedCallback(new OnActionOpened() {
            @Override
            public void actionOpened() {
                Toast.makeText(getApplicationContext(), "action opened", Toast.LENGTH_SHORT).show();
            }
        });
        //Callbacks - end

        actionSDK.actionDisplayed("sawmo");


    }

    @Override
    public void onResume() {
        super.onResume();
        if (streamActionsSwitch.isChecked()) {
            actionSDK.startMomentCallback(ActionsSdkMainActivity.this);
        } else {
            actionSDK.stopMomentCallback();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        actionSDK.stopMomentCallback();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(actionSDK != null){
            actionSDK.onDestroy();
        }
        if(interstitial != null) {
            interstitial.destroy();
        }
    }

    @Override
    public void onReceive(ArrayList<ActionData> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            Toast.makeText(getApplicationContext(), arrayList.get(i).getActionName() + " " + arrayList.get(i).getActionParam(), Toast.LENGTH_LONG).show();
            streamActions.add(arrayList.get(i).getActionParam());
        }
    }

    @Override
    public void onActionsLoaded(ArrayList<ActionData> arrayList) {
        if (getApplicationContext() != null) {
            loader.setVisibility(View.GONE);
            actions = arrayList;
        }
        Toast.makeText(getApplicationContext(), "Actions Loaded", Toast.LENGTH_SHORT).show();
        actionsAdapter = new ItemListAdapter(getApplicationContext(), R.layout.list_item, actions);
        ListView actionsListView = findViewById(R.id.actions_list);
        actionsListView.setVisibility(View.VISIBLE);
        actionsListView.setAdapter(actionsAdapter);
        actionsAdapter.notifyDataSetChanged();
        actionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                actionSDK.showAction(actions.get(i).getActionParam());
            }
        });
        actionsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                actionSDK.actionDisplayed(actions.get(i).getActionParam());
                Toast.makeText(getApplicationContext(), actions.get(i).getActionParam() + " action displayed", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    private void testOtherAppnextSDKs() {
        //Interstitial
        interstitial = new Interstitial(ActionsSdkMainActivity.this, "51eea790-c8be-4219-a45b-f32acb9e0e0b");
        interstitial.loadAd();
        interstitial.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s) {
                interstitial.showAd();
            }
        });
        //Native Ads
        AppnextAPI api = new AppnextAPI(ActionsSdkMainActivity.this, "51eea790-c8be-4219-a45b-f32acb9e0e0b");
        api.setAdListener(new AppnextAPI.AppnextAdListener() {
            @Override
            public void onAdsLoaded(ArrayList<AppnextAd> arrayList) {
                Toast.makeText(ActionsSdkMainActivity.this, "native ads test: " + arrayList.get(0).getAdTitle(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String s) {
                Toast.makeText(ActionsSdkMainActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        });
        api.loadAds(new AppnextAdRequest().setCount(500));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ActionsSdkMainActivity.this, MainActivity.class));
        finish();
    }
}
