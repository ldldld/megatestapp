package megaapp.qa.appnext.com.megatestapp.configuration;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedVideo;
import com.appnext.ads.fullscreen.Video;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.base.Appnext;
import com.appnext.core.Ad;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
import com.appnext.core.callbacks.OnVideoEnded;
import com.google.android.gms.ads.AdListener;

import java.util.HashMap;

import megaapp.qa.appnext.com.megatestapp.androidsdk.AndroidSdkMainActivity;
import megaapp.qa.appnext.com.megatestapp.R;

public class SdkFsInterRewardConfiguration extends AppCompatActivity implements View.OnClickListener{
    // Re Engage from MATAN
    //public static final String PID = "9d12d250-4c58-42f1-a841-b559cf578018"; // placement id
    // Sonic and evernote
    //public static final String PID = "cafeaa1f-981d-4e25-8c53-b2ab9dba4f86"; // placement id
    //public static final String PID = "45d1228d-ce63-420d-8986-9b0e1b912a72"; // placement id
    //public static final String PID = "2a086513-b307-44ea-b5cb-cf33c010c48e"; // placement id
    // From Royi
    //public static final String PID = "b667a6ba-9bb0-4a90-ad6b-97b9a9e3728b"; // placement id
    //public static final String PID = "686ac162-32d8-4fd4-a2f6-61fd7e3ebd58"; // placement id
    //public static final String PID = "4c8fd5c8-0ed1-411a-95fd-709d88d4fda7"; // placement id

    //public static final String PID = "91a5339c-c691-4568-887b-a4f5d19221c7"; // cpc hangman cpt_list new
    //public static final String PID = "17cb7b70-1d79-445c-9bce-10328110cfa5"; // cpc hangman cpt_list existing
    //public static final String PID = "1776a7dc-eb7c-4295-a401-b4a0565bed37"; // cpc hangman cpt_list new2
    //public static final String PID = "ebbd7534-70b1-4aa0-95b1-5d680002b5bf"; // cpc hangman cpt_list new2
    //public static final String PID = "848a8628-8e72-458c-be1c-d6055bb08dba"; // webView isWebView test
    public final String PID = "2f4cefe7-87d2-48cf-8b17-4abcd5c2aeff"; // Interstitial
    //public final String PID = "1ec77068-771b-4f86-95c1-504e9f308b76"; //
    //public final String PID = "f36685ea-3716-450c-bfbc-12f631d1477d"; // Amit Test

    private final String CUSTOM_EVENT = "default";
    private final String CUSTOMIZED_EVENT = "custom";
    private final String SERVER_EVENT = "server";
    private final String INTERSTITIAL_TYPE = "interstitial";
    private final String FULLSCREEN_TYPE = "fullScreen";
    private final String REWARDED_TYPE = "rewarded";

    private View viewLayout;
    private Button showDefaultEvent;
    private Button showCustomized;
    private Button showServer;

    private Button colorBtn;

    private Switch muteSwitch;
    private Switch autoPlaySwitch;
    private Switch backSwitch;

    private SeekBar seekRed;
    private SeekBar seekGreen;
    private SeekBar seekBlue;

    private EditText skipText;
    private EditText postBackText;
    private EditText categoriesText;
    private EditText maxVideoLengthET;
    private EditText minVideoLengthET;
    private EditText showCloseTime;
    private EditText multiTimer;
    private EditText rollCaptionTime;

    private EditText serverSidePostBack1;
    private EditText serverSidePostBack2;
    private EditText serverSidePostBack3;
    private EditText serverSidePostBack4;
    private EditText serverSidePostBack5;
    private String creativeTypeSelected;
    private String orientationSelected;
    private String colorStr;
    // private String originEvent;
    private String videoModeSelected;
    private String videoLengthSelected;

    private Interstitial interstitial;
    private InterstitialConfig interstitialConfig;
    //private InterstitialConfig interstitialConfig;

    private FullScreenVideo fullScreen;
    private FullscreenConfig fullScreenConfig;
    //private FullscreenConfig fullScreenConfig;

    private RewardedVideo rewarded;
    private RewardedConfig rewardedConfig;

    private boolean showCta;
    private boolean showClose;

    private int redInt = -1;
    private int greenInt = -1;
    private int blueInt = -1;
    private static HashMap<String, Object> interstitialConfigHash;
    private static HashMap<String, Object> interstitialHash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial_config);
        //createCreatures();
        //interstitial = InterstitialSettersCreator.interstitialSettersCreator(PID, SdkFsInterRewardConfiguration.this);

        interstitialConfigHash = new HashMap<>();
        //interstitialConfigHash = InterstitialConfigCreator.interstitialConfigCreator(PID,SdkFsInterRewardConfiguration.this);
        interstitialConfigHash.get("interstitialConfig");


        Button loadServer = findViewById(R.id.load_server);
        loadServer.setOnClickListener(this);
        showServer = findViewById(R.id.show_server);
        showServer.setOnClickListener(this);
        showServer.setEnabled(false);

        colorBtn =  findViewById(R.id.buttonColorSelection);
        colorBtn.setOnClickListener(this);

        serverSidePostBack1 = findViewById(R.id.serverSidePostBack1);
        serverSidePostBack2 = findViewById(R.id.serverSidePostBack2);
        serverSidePostBack3 = findViewById(R.id.serverSidePostBack3);
        serverSidePostBack4 = findViewById(R.id.serverSidePostBack4);
        serverSidePostBack5 = findViewById(R.id.serverSidePostBack5);
        TextView serverSidePostBackTitle = findViewById(R.id.serverSidePostBackTitle);

        Switch cacheVideo = findViewById(R.id.cache_video_switch);
        Switch streamingMode = findViewById(R.id.streaming_mode_switch);

        cacheVideo.setChecked(Video.getCacheVideo());

        cacheVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    Video.setCacheVideo(true);
                else
                    Video.setCacheVideo(false);
            }
        });

        streamingMode.setChecked(Video.isStreamingModeEnabled());

        streamingMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    Video.setStreamingMode(true);
                else
                    Video.setStreamingMode(false);
            }
        });


        Spinner orientationSpinner = findViewById(R.id.orientationSpinner);
        orientationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                orientationSelected = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        skipText =  findViewById(R.id.skipText);
        postBackText =  findViewById(R.id.postBackText);
        categoriesText = findViewById(R.id.categoriesText);
        maxVideoLengthET = findViewById(R.id.maxVideoLengthET);
        minVideoLengthET = findViewById(R.id.minVideoLengthET);

        multiTimer = findViewById(R.id.multiTimer);

        showCloseTime = findViewById(R.id.showCloseTime);

        rollCaptionTime = findViewById(R.id.rollCaptionTime);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Spinner creativeTypeSpinner = findViewById(R.id.creativeTypeSpinner);
        creativeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                creativeTypeSelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner videoMode = findViewById(R.id.videoModeSpinner);
        videoMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                videoModeSelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner videoLengthSpinner = findViewById(R.id.videoLengthSpinner);
        videoLengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                videoLengthSelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        muteSwitch = findViewById(R.id.muteSwitch);
        muteSwitch.setChecked(false);
        muteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        autoPlaySwitch = findViewById(R.id.autoPlaySwitch);
        autoPlaySwitch.setChecked(false);
        autoPlaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            boolean selectionB;
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectionB = autoPlaySwitch.isChecked();
                Toast.makeText(SdkFsInterRewardConfiguration.this, String.valueOf(selectionB), Toast.LENGTH_SHORT).show();
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        backSwitch = findViewById(R.id.backSwitch);
        backSwitch.setChecked(false);
        backSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        Button loadDefualtEvent = findViewById(R.id.load_custom_event);
        loadDefualtEvent.setOnClickListener(this);

        showDefaultEvent = findViewById(R.id.show_custom_event);
        showDefaultEvent.setOnClickListener(this);
        showDefaultEvent.setEnabled(false);

        Button loadCustomized = findViewById(R.id.load_customized);
        loadCustomized.setOnClickListener(this);

        showCustomized = findViewById(R.id.show_customized);
        showCustomized.setOnClickListener(this);
        showCustomized.setEnabled(false);

        final Switch showCloseSwitch = findViewById(R.id.showClose);
        showCloseSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showClose = showCloseSwitch.isChecked();
            }
        });

        final Switch showCtaSwitch = findViewById(R.id.showCta);
        showCtaSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showCta = showCtaSwitch.isChecked();
            }
        });
//        originEvent = getIntent().getStringExtra("origin");
        switch (originHandler()){
            case(INTERSTITIAL_TYPE):
               // backSwitch.setVisibility(View.GONE);
                showCloseTime.setVisibility(View.GONE);
                showCloseSwitch.setVisibility(View.GONE);
                videoMode.setVisibility(View.GONE);
                multiTimer.setVisibility(View.GONE);
                videoLengthSpinner.setVisibility(View.GONE);
                rollCaptionTime.setVisibility(View.GONE);
                showCtaSwitch.setVisibility(View.GONE);
                serverSidePostBack1.setVisibility(View.GONE);
                serverSidePostBack2.setVisibility(View.GONE);
                serverSidePostBack3.setVisibility(View.GONE);
                serverSidePostBack4.setVisibility(View.GONE);
                serverSidePostBack5.setVisibility(View.GONE);
                serverSidePostBackTitle.setVisibility(View.GONE);
                break;
            case(FULLSCREEN_TYPE):
                videoMode.setVisibility(View.GONE);
                skipText.setVisibility(View.GONE);
                creativeTypeSpinner.setVisibility(View.GONE);
                autoPlaySwitch.setVisibility(View.GONE);
                colorBtn.setVisibility(View.GONE);
                multiTimer.setVisibility(View.GONE);
                serverSidePostBack1.setVisibility(View.GONE);
                serverSidePostBack2.setVisibility(View.GONE);
                serverSidePostBack3.setVisibility(View.GONE);
                serverSidePostBack4.setVisibility(View.GONE);
                serverSidePostBack5.setVisibility(View.GONE);
                serverSidePostBackTitle.setVisibility(View.GONE);
                break;
            case(REWARDED_TYPE):
                showCloseTime.setVisibility(View.GONE);
                skipText.setVisibility(View.GONE);
                creativeTypeSpinner.setVisibility(View.GONE);
                autoPlaySwitch.setVisibility(View.GONE);
                colorBtn.setVisibility(View.GONE);
                backSwitch.setVisibility(View.GONE);
                showCloseSwitch.setVisibility(View.GONE);
                break;
        }
    }

    // END OF onCreate ()

    private String originHandler(){
        return getIntent().getStringExtra("origin");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            // Custom event
            case(R.id.load_custom_event):
                switch (originHandler()){
                    case(INTERSTITIAL_TYPE):
                        interstitialWithSetters();
                        break;
                    case(FULLSCREEN_TYPE):
                        fullScreenWithSetters();
                        break;
                    case(REWARDED_TYPE):
                        rewardedWithSetters();
                        break;
                }
                break;
            case(R.id.show_custom_event):
                switch (originHandler()){
                    case(INTERSTITIAL_TYPE):
                        if(interstitial != null){
                            if(interstitial.isAdLoaded()) {
                                interstitial.showAd();
                            }
                        }
//                        else{
//                            showDefaultEvent.setEnabled(false);
//                        }
                        break;
                    case(FULLSCREEN_TYPE):
                        if(fullScreen != null){
                            if(fullScreen.isAdLoaded()) {
                                fullScreen.showAd();
                            }
                        }
//                        else{
//                            showDefaultEvent.setEnabled(false);
//                        }
                        break;
                    case(REWARDED_TYPE):
                        if(rewarded != null){
                            if(rewarded.isAdLoaded()){
                                rewarded.showAd();
                            }
                        }
//                        else{
//                            showDefaultEvent.setEnabled(false);
//                        }
                        break;
                }
                break;
            // Configured event
            case(R.id.load_customized):
                switch (originHandler()){
                    case(INTERSTITIAL_TYPE):
                        interstitialWithConfig();
                        break;

                    case(FULLSCREEN_TYPE):
                        fullScreenWithConfig();
                        break;

                    case(REWARDED_TYPE):
                        rewardedWithConfig();
                        break;
                }
                break;
            case(R.id.show_customized):
                switch (originHandler()){
                    case(INTERSTITIAL_TYPE):
                        if( interstitial != null){
                            if(interstitial.isAdLoaded()) {
                                interstitial.showAd();
                            }
                        }
//                        else{
//                            showCustomized.setEnabled(false);
//                        }
                        break;
                    case(FULLSCREEN_TYPE):
                        if(fullScreen != null){
                            if(fullScreen.isAdLoaded()) {
                                fullScreen.showAd();
                            }
                        }
//                        else{
//                            showCustomized.setEnabled(false);
//                        }
                        break;
                    case(REWARDED_TYPE):
                        if(rewarded != null){
                            if(rewarded.isAdLoaded()) {
                                rewarded.showAd();
                            }
                        }
//                        else{
//                            showCustomized.setEnabled(false);
//                        }
                        break;
                }
                break;

            // Default event
            case(R.id.load_server):
                switch (originHandler()){
                    case(INTERSTITIAL_TYPE):
                        // interstitial = new Interstitial(this, PID);
                        backToDefaultDefinitionsInterstitial();
                        setCallBacks(interstitial, SERVER_EVENT, INTERSTITIAL_TYPE);
                        interstitial.loadAd();
                        break;
                    case(FULLSCREEN_TYPE):
                        // fullScreen = new FullScreenVideo(this,PID);
                        backToDefaultDefinitionsFullScreen();
                        setCallBacks(fullScreen, SERVER_EVENT, FULLSCREEN_TYPE);
                        fullScreen.loadAd();
                        break;
                    case(REWARDED_TYPE):
                        //rewarded = new RewardedVideo(this,PID);
                        backToDefaultDefinitionsRewarded();
                        setCallBacks(rewarded, SERVER_EVENT, REWARDED_TYPE);
                        rewarded.loadAd();
                        break;
                }
                break;
            case(R.id.show_server):
                switch (originHandler()){
                    case(INTERSTITIAL_TYPE):
                        if(interstitial != null){
                            if(interstitial.isAdLoaded()) {
                                interstitial.showAd();
                            }
                        }
//                        else{
//                            showServer.setEnabled(false);
//                        }
                        break;
                    case(FULLSCREEN_TYPE):
                        if(fullScreen != null){
                            if(fullScreen.isAdLoaded()) {
                                fullScreen.showAd();
                            }
                        }
// else{
//                            showServer.setEnabled(false);
//                        }
                        break;
                    case(REWARDED_TYPE):
                        if(rewarded != null){
                            if(rewarded.isAdLoaded()) {
                                rewarded.showAd();
                            }
                        }
//                        else{
//                            showServer.setEnabled(false);
//                        }
                        break;
                }
                break;
            case(R.id.buttonColorSelection):
                showDialog();
                break;
        }
    }

    public void interstitialWithSetters(){
        interstitial = new Interstitial(this, PID);

        interstitial.setLanguage("KO");
        interstitial.setAutoPlay(autoPlaySwitch.isChecked());
        interstitial.setBackButtonCanClose(backSwitch.isChecked());
        interstitial.isBackButtonCanClose();
        //Toast.makeText(this, "isBackButtonCanClose", Toast.LENGTH_SHORT).show();
        interstitial.setMute(muteSwitch.isChecked());
        switch (creativeTypeSelected){
            case("All"):
                interstitial.setCreativeType("managed");
                break;
            case("Static"):
                interstitial.setCreativeType("static");
                break;
            case("Video"):
                interstitial.setCreativeType("video");
                break;
            case("Creative"):
                interstitial.setCreativeType("managed");
                break;
        }
        interstitial.setOrientation(orientationSelectorConfig());
        interstitial.setSkipText(skipText.getText().toString());
        interstitial.setButtonColor(colorStr);
        interstitial.setPostback(postBackText.getText().toString());
        interstitial.setCategories(categoriesText.getText().toString());
        interstitial.setMaxVideoLength(maxVideoLengthET.getInputType());
        interstitial.setMinVideoLength(minVideoLengthET.getInputType());

        setCallBacks(interstitial, CUSTOM_EVENT, INTERSTITIAL_TYPE);
        interstitial.loadAd();
    }

    public void interstitialWithConfig(){
        //   ((Interstitial) interstitialConfigHash.get("interstitial")).destroy();
        interstitialConfig = new InterstitialConfig();

        //interstitialConfig = ((InterstitialConfig) interstitialConfigHash.get("interstitialConfig"));
        interstitialConfig.setAutoPlay(autoPlaySwitch.isChecked());
        interstitialConfig.setAutoPlay(autoPlaySwitch.isChecked());
        //Toast.makeText(this, "isBackButtonCanClose", Toast.LENGTH_SHORT).show();
        interstitialConfig.setBackButtonCanClose(backSwitch.isChecked());
        interstitialConfig.isBackButtonCanClose();
        interstitialConfig.setMute(muteSwitch.isChecked());
        interstitialConfig.setLanguage("KO");

        switch (creativeTypeSelected){
            case("All"):
                interstitialConfig.setCreativeType("managed");
                break;
            case("Static"):
                interstitialConfig.setCreativeType("static");
                break;
            case("Video"):
                interstitialConfig.setCreativeType("video");
                break;
        }
        interstitialConfig.setOrientation(orientationSelectorConfig());
        interstitialConfig.setSkipText(skipText.getText().toString());
        interstitialConfig.setButtonColor(colorStr);
        interstitialConfig.setPostback(postBackText.getText().toString());
        interstitialConfig.setCategories(categoriesText.getText().toString());
        interstitialConfig.setMaxVideoLength(maxVideoLengthET.getInputType());
        interstitialConfig.setMinVideoLength(minVideoLengthET.getInputType());
        if(interstitial != null){
            interstitial.destroy();
        }
        interstitial = new Interstitial(this,PID, interstitialConfig);
        interstitial.setLanguage("KO");
        setCallBacks(interstitial, CUSTOMIZED_EVENT, INTERSTITIAL_TYPE);
        interstitial.loadAd();
    }

    public void fullScreenWithSetters(){
        fullScreen = new FullScreenVideo(this, PID);

        fullScreen.setLanguage("KO");
        fullScreen.setMute(muteSwitch.isChecked());
        fullScreen.setBackButtonCanClose(backSwitch.isChecked());
        fullScreen.setOrientation(orientationSelectorConfig());
        fullScreen.setPostback(postBackText.getText().toString());
        fullScreen.setCategories(categoriesText.getText().toString());
        fullScreen.setMaxVideoLength(maxVideoLengthET.getInputType());
        fullScreen.setMinVideoLength(minVideoLengthET.getInputType());
        fullScreen.setLanguage("KO");
        if(videoLengthSelected.equals("Video Length")){
            fullScreen.setVideoLength(Video.VIDEO_LENGTH_SHORT);
        }else{
            if (videoLengthSelected.equals("Short - 15 s.")){
                fullScreen.setVideoLength(Video.VIDEO_LENGTH_SHORT);
            }
            if (videoLengthSelected.equals("Long - 30 s.")){
                fullScreen.setVideoLength(Video.VIDEO_LENGTH_LONG);
            }
        }
        fullScreen.setRollCaptionTime(rollCaptionTime.getInputType());
        fullScreen.setShowCta(showCta);
        if (!showCloseTime.getText().toString().equals("") && !showClose){
            fullScreen.setShowClose(true, Integer.parseInt(showCloseTime.getText().toString()));
        }
        if (showCloseTime.getText().toString().equals("") && !showClose){
            Log.i("Default values ", "Loaded");
            // fullScreen.setShowClose(false);
        }else{
            fullScreen.setShowClose(showClose, Integer.parseInt(showCloseTime.getText().toString()));
        }
        setCallBacks(fullScreen, CUSTOM_EVENT, FULLSCREEN_TYPE);
        fullScreen.loadAd();
    }

    public void fullScreenWithConfig(){
        FullscreenConfig fullScreenConfig = new FullscreenConfig();
        fullScreenConfig.setMute(muteSwitch.isChecked());
        fullScreenConfig.setOrientation(orientationSelectorConfig());
        fullScreenConfig.setPostback(postBackText.getText().toString());
        fullScreenConfig.setCategories(categoriesText.getText().toString());
        fullScreenConfig.setMaxVideoLength(maxVideoLengthET.getInputType());
        fullScreenConfig.setMinVideoLength(minVideoLengthET.getInputType());
        fullScreenConfig.setLanguage("KO");
        if(videoLengthSelected.equals("Video Length")){
            fullScreenConfig.setVideoLength(Video.VIDEO_LENGTH_SHORT);
        }else{
            if (videoLengthSelected.equals("Short - 15 s.")){
                fullScreenConfig.setVideoLength(Video.VIDEO_LENGTH_SHORT);
            }
            if (videoLengthSelected.equals("Long - 30 s.")){
                fullScreenConfig.setVideoLength(Video.VIDEO_LENGTH_LONG);
            }
        }
        fullScreenConfig.setRollCaptionTime(rollCaptionTime.getInputType());
        fullScreenConfig.setShowCta(showCta);
        if (!showCloseTime.getText().toString().equals("") && !showClose){
            fullScreenConfig.setShowClose(true, Integer.parseInt(showCloseTime.getText().toString()));
        }
        if (showCloseTime.getText().toString().equals("") && !showClose){
            Log.i("Default values ", "Loaded");
            //fullScreenConfig.setShowClose(false);
        }else{
            fullScreenConfig.setShowClose(showClose, Integer.parseInt(showCloseTime.getText().toString()));
        }
        if(fullScreen != null){
            fullScreen.destroy();
        }
        fullScreen = new FullScreenVideo(this, PID, fullScreenConfig);
        fullScreen.setLanguage("KO");
        setCallBacks(fullScreen, CUSTOMIZED_EVENT, FULLSCREEN_TYPE);
        fullScreen.loadAd();

    }

    public void rewardedWithSetters(){
        rewarded = new RewardedVideo(this, PID);
        rewarded.setMute(muteSwitch.isChecked());
        rewarded.setLanguage("KO");
        if(videoModeSelected.equals("Video Mode")){
            videoModeSelected = "multi";
        }else{
            videoModeSelected = videoModeSelected.toLowerCase();
        }
        rewarded.setMode(videoModeSelected);

        rewarded.setMultiTimerLength(multiTimer.getInputType());
        rewarded.setOrientation(orientationSelectorConfig());
        rewarded.setPostback(postBackText.getText().toString());
        rewarded.setCategories(categoriesText.getText().toString());
        if(videoLengthSelected.equals("Video Length")){
            rewarded.setVideoLength(Video.VIDEO_LENGTH_SHORT);
        }else{
            if (videoLengthSelected.equals("Short - 15 s.")){
                rewarded.setVideoLength(Video.VIDEO_LENGTH_SHORT);
            }
            if (videoLengthSelected.equals("Long - 30 s.")){
                rewarded.setVideoLength(Video.VIDEO_LENGTH_LONG);
            }
        }
        rewarded.setMaxVideoLength(maxVideoLengthET.getInputType());
        rewarded.setMinVideoLength(minVideoLengthET.getInputType());
        rewarded.setRollCaptionTime(rollCaptionTime.getInputType());
        rewarded.setShowCta(showCta);
        rewarded.setRewardedServerSidePostback(serverSidePostBack1.getText().toString(),
                serverSidePostBack2.getText().toString(),
                serverSidePostBack3.getText().toString(),
                serverSidePostBack4.getText().toString(),
                serverSidePostBack5.getText().toString());
        rewarded.setLanguage("KO");
        setCallBacks(rewarded, CUSTOM_EVENT, REWARDED_TYPE);
        rewarded.loadAd();

    }

    public void rewardedWithConfig(){
     rewardedConfig = new RewardedConfig();
        rewardedConfig.setMute(muteSwitch.isChecked());
        rewardedConfig.setLanguage("KO");
        if(videoModeSelected.equals("Video Mode")){
            videoModeSelected = "normal";
        }else{
            videoModeSelected = videoModeSelected.toLowerCase();
        }
        rewardedConfig.setMode(videoModeSelected);

        rewardedConfig.setMultiTimerLength(multiTimer.getInputType());
        rewardedConfig.setOrientation(orientationSelectorConfig());
        rewardedConfig.setPostback(postBackText.getText().toString());
        rewardedConfig.setCategories(categoriesText.getText().toString());
        if(videoLengthSelected.equals("Video Length")){
            rewardedConfig.setVideoLength(Video.VIDEO_LENGTH_SHORT);
        }else{
            if (videoLengthSelected.equals("Short - 15 s.")){
                rewardedConfig.setVideoLength(Video.VIDEO_LENGTH_SHORT);
            }
            if (videoLengthSelected.equals("Long - 30 s.")){
                rewardedConfig.setVideoLength(Video.VIDEO_LENGTH_LONG);
            }
        }
        rewardedConfig.setMaxVideoLength(maxVideoLengthET.getInputType());
        rewardedConfig.setMinVideoLength(minVideoLengthET.getInputType());
        rewardedConfig.setRollCaptionTime(rollCaptionTime.getInputType());
        rewardedConfig.setShowCta(showCta);
        if(rewarded != null){
            rewarded.destroy();
        }
        rewarded = new RewardedVideo(this, PID, rewardedConfig);
        rewarded.setLanguage("KO");
        setCallBacks(rewarded, CUSTOMIZED_EVENT, REWARDED_TYPE);
        rewarded.loadAd();

    }

    private String orientationSelectorConfig() {
        String orientation = "not_set";
        switch (orientationSelected){
            case("Auto"):
                orientation = "automatic";
                break;
            case("Default"):
                orientation = "not_set";
                break;
            case("Landscape"):
                orientation = "landscape";
                break;
            case("Portrait"):
                orientation = "portrait";
                break;
        }
        return orientation;
    }

    // Back to default settings
    public void backToDefaultDefinitionsInterstitial() {
        interstitial = new Interstitial(this, PID);
        interstitial.setMute(false);
        interstitial.setOrientation(Video.ORIENTATION_DEFAULT);
        interstitial.setButtonColor("");
        interstitial.setPostback("");
        interstitial.setCategories("");
        interstitial.setMaxVideoLength(0);
        interstitial.setMinVideoLength(0);
        interstitial.setBackButtonCanClose(false);
        interstitial.isBackButtonCanClose();
        //Toast.makeText(this, "isBackButtonCanClose", Toast.LENGTH_SHORT).show();
        interstitial.setSkipText("");
        interstitial.setCreativeType("managed");
        interstitial.setAutoPlay(true);
    }
    public void backToDefaultDefinitionsFullScreen() {
        fullScreen = new FullScreenVideo(this, PID);
        fullScreen.setMute(false);
        fullScreen.setBackButtonCanClose(false);
        fullScreen.setOrientation(Video.ORIENTATION_DEFAULT);
        fullScreen.setPostback("");
        fullScreen.setCategories("");
        fullScreen.setMaxVideoLength(0);
        fullScreen.setMinVideoLength(0);
        fullScreen.setVideoLength("15");
        fullScreen.setRollCaptionTime(-2);
        fullScreen.setShowCta(true);
        fullScreen.setShowClose(true);
        fullScreen.setShowClose(true, 0);
    }
    public void backToDefaultDefinitionsRewarded() {
        rewarded = new RewardedVideo(this, PID);
        rewarded.setMute(false);
        rewarded.setMode("default");
        rewarded.setOrientation(Video.ORIENTATION_DEFAULT);
        rewarded.setPostback("");
        rewarded.setCategories("");
        rewarded.setMaxVideoLength(0);
        rewarded.setMinVideoLength(0);
        rewarded.setMultiTimerLength(5);
        rewarded.setRewardedServerSidePostback("", "", "", "", "");
        rewarded.setVideoLength("15");
        rewarded.setRollCaptionTime(-2);
        rewarded.setShowCta(true);
    }

    public void setCallBacks(Ad anyAd, final String callingEvent, final String eventType){
        anyAd.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s) {
                switch (callingEvent){
                    case (CUSTOM_EVENT):

                        showDefaultEvent.setEnabled(true);
                        break;

                    case (CUSTOMIZED_EVENT):
                        showCustomized.setEnabled(true);
                        break;

                    case (SERVER_EVENT):
                        showServer.setEnabled(true);
                        break;
                }

                Toast.makeText(SdkFsInterRewardConfiguration.this, "Loaded " + eventType, Toast.LENGTH_SHORT).show();
            }
        });

        anyAd.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {
                Toast.makeText(SdkFsInterRewardConfiguration.this, "Opened " + eventType, Toast.LENGTH_SHORT).show();
            }
        });

        anyAd.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {
                Toast.makeText(SdkFsInterRewardConfiguration.this, "Clicked " + eventType, Toast.LENGTH_SHORT).show();
            }
        });

        anyAd.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                Toast.makeText(SdkFsInterRewardConfiguration.this, "Closed " + eventType, Toast.LENGTH_SHORT).show();
                Log.i("@@@@ destroy reached", "destroy");
                switch (callingEvent){
                    case (CUSTOM_EVENT):
                        showDefaultEvent.setEnabled(false);
                        showCustomized.setEnabled(false);
                        showServer.setEnabled(false);
                        break;

                    case (CUSTOMIZED_EVENT):
                        showDefaultEvent.setEnabled(false);
                        showCustomized.setEnabled(false);
                        showServer.setEnabled(false);
                        break;

                    case (SERVER_EVENT):
                        showDefaultEvent.setEnabled(false);
                        showCustomized.setEnabled(false);
                        showServer.setEnabled(false);
                        break;
                }

                // switch (eventType) {
                //   case (INTERSTITIAL_TYPE):
                if(interstitial != null) {
                    interstitial.destroy();
                    interstitial = null;
                }

//                if(interstitialConfigured != null) {
//                    interstitialConfigured.destroy();
//                    interstitialConfigured = null;
//                }
                //    break;

                // case (FULLSCREEN_TYPE):
                if(fullScreen != null) {
                    fullScreen.destroy();
                    fullScreen = null;
                }
//                if(fullScreenConfigured != null) {
//                    fullScreenConfigured.destroy();
//                    fullScreenConfigured = null;
//                }
                //   break;

                // case (REWARDED_TYPE):
                if(rewarded != null) {
                    rewarded.destroy();
                    rewarded = null;
                }
//                if(rewardedConfigured != null) {
//                    rewardedConfigured.destroy();
//                    rewardedConfigured = null;
//                }
                //  break;
                //}
            }
        });

        anyAd.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String s) {
                Toast.makeText(SdkFsInterRewardConfiguration.this, "Error: " + s, Toast.LENGTH_SHORT).show();
                showDefaultEvent.setEnabled(false);
                showCustomized.setEnabled(false);
                showServer.setEnabled(false);
            }
        });

        if(anyAd instanceof RewardedVideo){
            ((RewardedVideo) anyAd).setOnVideoEndedCallback(new OnVideoEnded() {
                @Override
                public void videoEnded() {
                    Toast.makeText(SdkFsInterRewardConfiguration.this, "Video Ended", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setColor() {
        String red;
        String green;
        String blue;

        try {
            viewLayout.setBackgroundColor(Color.rgb(seekRed.getProgress(), seekGreen.getProgress(), seekBlue.getProgress()));
            int color = Color.rgb(seekRed.getProgress(), seekGreen.getProgress(), seekBlue.getProgress());
            if(Integer.toHexString(seekRed.getProgress()).length() < 2){
                red = "0" + Integer.toHexString(seekRed.getProgress());
            }else{
                red = Integer.toHexString(seekRed.getProgress());
            }
            if(Integer.toHexString(seekGreen.getProgress()).length() < 2){
                green = "0" + Integer.toHexString(seekGreen.getProgress());
            }else {
                green = Integer.toHexString(seekGreen.getProgress());
            }
            if(Integer.toHexString(seekBlue.getProgress()).length() < 2){
                blue = "0" + Integer.toHexString(seekBlue.getProgress());
            }else {
                blue = Integer.toHexString(seekBlue.getProgress());
            }

            redInt = seekRed.getProgress();
            greenInt = seekGreen.getProgress();
            blueInt = seekBlue.getProgress();

            colorStr = red + green + blue;
//            colorBtn.setBackgroundResource(R.drawable.customborder);
//            colorBtn.setBackgroundColor(Color.rgb(seekRed.getProgress(), seekGreen.getProgress(), seekBlue.getProgress()));

            GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{color, color});
            gradient.setShape(GradientDrawable.RECTANGLE);
            gradient.setCornerRadius(80);
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            colorBtn.setBackground(gradient);
            //}else {
            //  colorBtn.setBackground(gradient);
            //}
        } catch (IllegalArgumentException e) {
            Log.i("Wrong color value: ", e.toString());
        }
    }
    public void showDialog(){

        AlertDialog.Builder popDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

        viewLayout = inflater != null ? inflater.inflate(R.layout.color_selection,
                (ViewGroup) findViewById(R.id.layout_dialog)) : null;

        //popDialog.setIcon(android.R.drawable.btn_star_big_on);

        popDialog.setTitle("Please Select Rank 0-255 for each color ");
        popDialog.setView(viewLayout);
        viewLayout.setBackgroundColor(Color.parseColor("#7D7D7D"));


        //  seekBar1
        seekRed =  viewLayout.findViewById(R.id.seekBar1);

        seekRed.setMax(255);
        if (redInt != -1 && redInt != 125){
            seekRed.setProgress(redInt);
        }else {
            seekRed.setProgress(125);
        }

        seekRed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                if (seekRed.getProgress() <= 0) {
                    seekRed.setProgress(0);
                }
                setColor();
                //viewLayout.setBackgroundColor(Color.parseColor("#"+ Integer.toString(progress, 16) + Integer.toString(seekGreen.getProgress(),16) + Integer.toString(seekBlue.getProgress(),16)));
                //seekRed.getProgress();

                //Do something here with new value
                //item1.setText("Value of : " + progress);
            }

            public void onStartTrackingTouch(SeekBar arg0) {
                Log.i("onStartTrackingTouch ", "onStartTrackingTouch");
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //  seekBar2
        seekGreen = viewLayout.findViewById(R.id.seekBar2);
        seekGreen.setMax(255);
        seekRed.setMax(255);

        if (greenInt != -1 && greenInt != 125){
            seekGreen.setProgress(greenInt);
        }else {
            seekGreen.setProgress(125);
        }

        seekGreen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                if (seekGreen.getProgress() <= 0) {
                    seekGreen.setProgress(0);
                }
                setColor();

//                viewLayout.setBackgroundColor(Color.parseColor("#"+ Integer.toString(progress, 16) + Integer.toString(seekGreen.getProgress(),16) + Integer.toString(seekBlue.getProgress(),16)));

                //Do something here with new value
                // item2.setText("Value of : " + progress);
            }

            public void onStartTrackingTouch(SeekBar arg0) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        //  seekBar3
        seekBlue = viewLayout.findViewById(R.id.seekBar3);
        seekBlue.setMax(255);

        if (blueInt != -1 && blueInt != 125){
            seekBlue.setProgress(blueInt);
        }else {
            seekBlue.setProgress(125);
        }

        seekBlue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                if (seekBlue.getProgress() <= 0) {
                    seekBlue.setProgress(0);
                }
                setColor();

//                viewLayout.setBackgroundColor(Color.parseColor("#"+ Integer.toString(progress, 16) + Integer.toString(seekGreen.getProgress(),16) + Integer.toString(seekBlue.getProgress(),16)));

                //Do something here with new value
                // item2.setText("Value of : " + progress);
            }

            public void onStartTrackingTouch(SeekBar arg0) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        viewLayout.setBackgroundColor(Color.rgb(seekRed.getProgress(), seekGreen.getProgress(), seekBlue.getProgress()));

        popDialog.setCancelable(false);
        // Button OK
        popDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }

                });

        popDialog.create();
        popDialog.show();

    }

//    private void killInstances(){
//
//    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SdkFsInterRewardConfiguration.this, AndroidSdkMainActivity.class));
        finish();
    }
}
