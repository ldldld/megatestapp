package megaapp.qa.appnext.com.megatestapp.actionssdk.actionsAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appnext.actionssdk.ActionData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import megaapp.qa.appnext.com.megatestapp.R;

/**
 * Created by leonid on 12/11/2017.
 */

public class ItemListAdapter extends ArrayAdapter<ActionData> {

    private Context context;

    public ItemListAdapter(Context context, int resource, List<ActionData> actions) {
        super(context, resource, actions);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        final ActionData item = getItem(position);
        ViewHolderItem viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null) {
                convertView = inflater.inflate(R.layout.list_item, parent, false);
            }

            viewHolder = new ViewHolderItem();
            if (convertView != null) {
                viewHolder.image = convertView.findViewById(R.id.action_icon);
                viewHolder.name = convertView.findViewById(R.id.action_name);
                viewHolder.param = convertView.findViewById(R.id.action_param);
                viewHolder.dynamicMessage = convertView.findViewById(R.id.action_dynamic_message);
                viewHolder.expireTime = convertView.findViewById(R.id.action_expire_time);
                convertView.setTag(viewHolder);
            }

        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        if (item != null) {
            viewHolder.name.setText(item.getActionName());
            viewHolder.param.setText(item.getActionParam());
            viewHolder.dynamicMessage.setText(item.getActionDynamicMessage());
            viewHolder.expireTime.setText(getDate(item.getExpireMillis()));
            viewHolder.image.setImageDrawable(item.getActionIcon(context));
        }


        return convertView;
    }

    static class ViewHolderItem {
        TextView name;
        TextView param;
        TextView dynamicMessage;
        TextView expireTime;
        ImageView image;
    }

    private String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
