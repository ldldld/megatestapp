package megaapp.qa.appnext.com.megatestapp.nativeadssdk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.appnextsdk.API.AppnextAPI;
import com.appnext.appnextsdk.API.AppnextAd;
import com.appnext.appnextsdk.API.AppnextAdRequest;
import com.appnext.core.AppnextError;
import com.appnext.core.callbacks.OnAdClicked;
import com.appnext.core.callbacks.OnAdClosed;
import com.appnext.core.callbacks.OnAdError;
import com.appnext.core.callbacks.OnAdLoaded;
import com.appnext.core.callbacks.OnAdOpened;
//import com.crashlytics.android.Crashlytics;
import java.util.ArrayList;
//import io.fabric.sdk.android.Fabric;
import megaapp.qa.appnext.com.megatestapp.MainActivity;
import megaapp.qa.appnext.com.megatestapp.R;

public class NativeAdsActivity extends AppCompatActivity implements AppnextCallbacks, View.OnClickListener {

    //public static final String pId = "4e290438-5bb6-43ba-b607-b123badffd61"; //make sure to change the placement id also in the videoActivity
    //public static final String pId = "cafeaa1f-981d-4e25-8c53-b2ab9dba4f86"; // cpi & cpc
    public static final String pId = "2a086513-b307-44ea-b5cb-cf33c010c48e"; // cpi & cpc
   // private final String pId2 = "51eea790-c8be-4219-a45b-f32acb9e0e0b";
    public static AppnextAPI appnext;
    ////////interstitial part//////////
    private Interstitial interstitial_default_ad;

    public ArrayList<AppnextAd> ads;
    public RecyclerView mRecyclerView;
    public MyAdapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_ads);
       // Appnext.init(this);

      //  Fabric.with(this, new Crashlytics());

        Button refreshButton = findViewById(R.id.refresh_button);
        refreshButton.setOnClickListener(this);
        mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        callInterstitial();

    }

    public void callInterstitial(){
        interstitial_default_ad = new Interstitial(NativeAdsActivity.this, pId);
        callBacks(interstitial_default_ad);
        interstitial_default_ad.loadAd();


    }

    private void callBacks(final Interstitial interstitial_default_ad){
        interstitial_default_ad.setOnAdClickedCallback(new OnAdClicked() {
            @Override
            public void adClicked() {
                Toast.makeText(NativeAdsActivity.this, "Interstitial Ad Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        interstitial_default_ad.setOnAdClosedCallback(new OnAdClosed() {
            @Override
            public void onAdClosed() {
                Toast.makeText(NativeAdsActivity.this, "Interstitial Ad Closed", Toast.LENGTH_SHORT).show();

            }
        });

        interstitial_default_ad.setOnAdErrorCallback(new OnAdError() {
            @Override
            public void adError(String errorString) {
                Toast.makeText(NativeAdsActivity.this, "Interstitial Ad Error " + errorString, Toast.LENGTH_SHORT).show();

            }
        });

        interstitial_default_ad.setOnAdLoadedCallback(new OnAdLoaded() {
            @Override
            public void adLoaded(String s) {
                interstitial_default_ad.showAd();
                Toast.makeText(NativeAdsActivity.this, "Interstitial Ad Loaded & Shown", Toast.LENGTH_SHORT).show();

            }
        });

        interstitial_default_ad.setOnAdOpenedCallback(new OnAdOpened() {
            @Override
            public void adOpened() {
                Toast.makeText(NativeAdsActivity.this, "Interstitial Ad Opened", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void callNativeAds(){
        appnext = new AppnextAPI(NativeAdsActivity.this, pId);

        appnext.setAdListener(new AppnextAPI.AppnextAdListener() {
            @Override
            public void onAdsLoaded(ArrayList<AppnextAd> ads) {
                NativeAdsActivity.this.ads = ads;
                Toast.makeText(getApplicationContext(), "Ad Loaded", Toast.LENGTH_SHORT).show();
                //   loader.setVisibility(View.GONE);
                mAdapter = new MyAdapter(NativeAdsActivity.this, ads);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                mAdapter.setAppnextCallbacks(NativeAdsActivity.this);
            }
            @Override
            public void onError(String s) {
                Toast.makeText(getApplicationContext(), "Error " + s, Toast.LENGTH_SHORT).show();
            }
        });

        appnext.setCreativeType(AppnextAPI.TYPE_VIDEO);
        appnext.loadAds(new AppnextAdRequest()
                .setMinVideoLength(1)
                .setCount(500)
                .setPostback("NativeAds - Android"));
        appnext.setParams("_cachingRequest", "2");
        appnext.setLanguage("KO");
        appnext.setOnAdOpenedListener(new AppnextAPI.OnAdOpened() {
            @Override
            public void storeOpened() {
                Toast.makeText(getApplicationContext(),"Ad Opened",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String s) {
                if (s.equals(AppnextError.NO_ADS)) {
                    Toast.makeText(NativeAdsActivity.this,"Error: " + s, Toast.LENGTH_SHORT).show();
                }
                if (s.equals(AppnextError.NO_MARKET)) {
                    Toast.makeText(NativeAdsActivity.this,"Error: " + s, Toast.LENGTH_SHORT).show();
                }
                if (s.equals(AppnextError.CONNECTION_ERROR)) {
                    Toast.makeText(NativeAdsActivity.this,"Error: " + s, Toast.LENGTH_SHORT).show();
                }
                if (s.equals(AppnextError.INTERNAL_ERROR)) {
                    Toast.makeText(NativeAdsActivity.this,"Error: " + s, Toast.LENGTH_SHORT).show();
                }
                if (s.equals(AppnextError.TIMEOUT)) {
                    Toast.makeText(NativeAdsActivity.this,"Error: " + s, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }



    @Override
    public void onAdClicked(AppnextAd ad) {
    }

    @Override
    public void onAdImpression(AppnextAd ad) {
        Log.d("adImp", ad.getAdTitle());
        appnext.adImpression(ad);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(interstitial_default_ad != null){
            interstitial_default_ad.destroy();
        }
        if(appnext != null) {
            appnext.destroy();
        }
        startActivity(new Intent(NativeAdsActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.refresh_button):
                callNativeAds();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(appnext != null) {
            appnext.destroy();
        }
    }
}
