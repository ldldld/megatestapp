package megaapp.qa.appnext.com.megatestapp.nativeadssdk;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.appnext.appnextsdk.API.AppnextAPI;
import com.appnext.appnextsdk.API.AppnextAd;

import megaapp.qa.appnext.com.megatestapp.R;

public class VideoActivity extends AppCompatActivity {

    //private final String pId = "b308acb6-462b-42f0-8ef3-6ba46aba7ef0"; //make sure to change the placement id also in the mainActivity
    public AppnextAPI appnext;
    public VideoView videoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;
    private String videoUrlString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_native_ads);

        appnext = new AppnextAPI(VideoActivity.this, NativeAdsActivity.pId);
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("videoUrl") != null) {
                videoUrlString = getIntent().getExtras().getString("videoUrl");
            }
        }
        final AppnextAd ad = (AppnextAd) getIntent().getExtras().getSerializable("ad");

        if (mediaControls == null) {
            mediaControls = new MediaController(this);
        }

        videoView = findViewById(R.id.video_view);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            videoView.setMediaController(mediaControls);
            videoView.setVideoURI(Uri.parse(videoUrlString));
        } catch (Exception e) {
            Log.e("Video Error", e.getMessage());
            e.printStackTrace();
        }

        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressDialog.dismiss();
                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                    appnext.videoStarted(ad);
                    Toast.makeText(VideoActivity.this, "Video Started", Toast.LENGTH_SHORT).show();
                } else {
                    videoView.pause();
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                appnext.videoEnded(ad);
                Toast.makeText(VideoActivity.this, "Video Ended", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("Position", videoView.getCurrentPosition());
        videoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        position = savedInstanceState.getInt("Position");
        videoView.seekTo(position);
    }

}
