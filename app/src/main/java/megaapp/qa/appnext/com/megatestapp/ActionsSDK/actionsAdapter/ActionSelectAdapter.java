package megaapp.qa.appnext.com.megatestapp.actionssdk.actionsAdapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import megaapp.qa.appnext.com.megatestapp.actionssdk.ActionCustomObject;
import megaapp.qa.appnext.com.megatestapp.R;

/**
 * Created by leonid on 12/11/2017.
 */

public class ActionSelectAdapter extends BaseAdapter {

    private List<ActionCustomObject> arrayList;
    private LayoutInflater inflater;

    public ActionSelectAdapter(Context context, List<ActionCustomObject> arrayList) {
        this.arrayList = arrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView textView;
        CheckBox checkBox;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_listview_multiple,  parent, false);
            holder.textView = convertView.findViewById(R.id.alert_textview);
            holder.checkBox = convertView.findViewById(R.id.alert_checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ActionCustomObject aco = arrayList.get(position);

        holder.textView.setText(aco.getName() + " (" + aco.getParam() + ")");
        holder.textView.setTypeface(null, Typeface.NORMAL);
        holder.checkBox.setChecked(aco.isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final ViewHolder temp = (ViewHolder) v.getTag();
                temp.checkBox.setChecked(!temp.checkBox.isChecked());

                aco.setSelected(!aco.isSelected());
            }
        });

        holder.checkBox.setTag(holder);
        return convertView;
    }
}
