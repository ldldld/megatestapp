package megaapp.qa.appnext.com.megatestapp.nativeadssdk;

import com.appnext.appnextsdk.API.AppnextAd;


public interface AppnextCallbacks {

    void onAdClicked(AppnextAd ad);
    void onAdImpression(AppnextAd ad);
//    void onVideoShown(AppnextAd ad);

}
