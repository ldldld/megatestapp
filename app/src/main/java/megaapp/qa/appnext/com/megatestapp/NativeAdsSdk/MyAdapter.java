package megaapp.qa.appnext.com.megatestapp.nativeadssdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.appnext.appnextsdk.API.AppnextAd;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import megaapp.qa.appnext.com.megatestapp.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    public ArrayList<AppnextAd> ads;
    public Context context;
    private AppnextCallbacks appnextCallbacks;





    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView adDescription, adPackage, adTitle, appSize, bannerID, campaignID, campaignType, categories, country, IDX, storeDownloads, storeRating, supportedVersion;
        private ImageView cardImage, infoImage;
        private Button installButton, playVideoButton, impressionButton,getEcpmButton;

        private ViewHolder(View itemView) {
            super(itemView);
            adTitle = itemView.findViewById(R.id.Ad_Title);
            adDescription = itemView.findViewById(R.id.Ad_Description);
            adPackage = itemView.findViewById(R.id.Ad_Package);
            appSize = itemView.findViewById(R.id.App_Size);
            bannerID =  itemView.findViewById(R.id.Banner_ID);
            campaignID =  itemView.findViewById(R.id.Campaign_ID);
            campaignType =  itemView.findViewById(R.id.Campaign_Type);
            categories =  itemView.findViewById(R.id.Categories);
            country =  itemView.findViewById(R.id.Country);
            IDX =  itemView.findViewById(R.id.IDX);
            storeDownloads =  itemView.findViewById(R.id.Store_Downloads);
            storeRating =  itemView.findViewById(R.id.Store_Rating);
            supportedVersion =  itemView.findViewById(R.id.Supported_Version);
            cardImage =  itemView.findViewById(R.id.card_image);
            installButton =  itemView.findViewById(R.id.install_Button);
            impressionButton =  itemView.findViewById(R.id.impression_button);
            playVideoButton =  itemView.findViewById(R.id.play_video_button);
            playVideoButton.setEnabled(false);
            getEcpmButton= itemView.findViewById(R.id.get_Ecpm_Button);
            infoImage =  itemView.findViewById(R.id.i_image);
            //getEcpmButton = (Button) itemView.findViewById(R.id.get_ecpm_button);
        }
    }

    MyAdapter(Context context, ArrayList<AppnextAd> ads) {
        this.context = context;
        this.ads = ads;

    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_item_native_ads, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int i) {
       // i = holder.getAdapterPosition();
        holder.cardImage.setImageBitmap(null);
        new DownloadImage(holder.cardImage).execute(ads.get(i).getImageURL());
        //holder.getEcpmButton.setText(ads.get(i).getButtonText());
        holder.adDescription.setText(ads.get(i).getAdDescription());
        holder.adPackage.setText(ads.get(i).getAdPackage());
        holder.adTitle.setText(ads.get(i).getAdTitle());
        holder.appSize.setText(ads.get(i).getAppSize());
        holder.bannerID.setText(ads.get(i).getBannerID());
        holder.campaignID.setText(ads.get(i).getCampaignID());
        holder.campaignType.setText(ads.get(i).getCampaignType());
        holder.categories.setText(ads.get(i).getCategories());
        holder.country.setText(ads.get(i).getCountry());
        holder.IDX.setText(ads.get(i).getIdx());
        holder.storeDownloads.setText(ads.get(i).getStoreDownloads());
        holder.storeRating.setText(ads.get(i).getStoreRating());
        holder.supportedVersion.setText(ads.get(i).getSupportedVersion());
        holder.installButton.setText(ads.get(i).getButtonText());
        holder.installButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NativeAdsActivity.appnext.adClicked(ads.get(holder.getAdapterPosition()));
                Log.d("adClicked", ads.get(holder.getAdapterPosition()).getAdTitle());
                if (appnextCallbacks != null) {
                    appnextCallbacks.onAdClicked(ads.get(holder.getAdapterPosition()));
                }
            }
        });

        holder.infoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NativeAdsActivity.appnext.privacyClicked(ads.get(holder.getAdapterPosition()));
            }
        });

        holder.impressionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appnextCallbacks != null) {
                    appnextCallbacks.onAdImpression(ads.get(holder.getAdapterPosition()));
                    Toast.makeText(context, "Impression Sent", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(context,"Ecpm "+ads.get(i).getECPM(),Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.getEcpmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, " ecpm " +
                                String.valueOf(ads.get(holder.getAdapterPosition()).getECPM())
                                + " ,PPR "
                                + String.valueOf(ads.get(holder.getAdapterPosition()).getPPR())
                                + " ,bannerId "
                                + String.valueOf(ads.get(holder.getAdapterPosition()).getBannerID()),
                        Toast.LENGTH_SHORT).show();
            }
        });

        //send imp on view
        appnextCallbacks.onAdImpression(ads.get(i));

        if (ads.get(i).getVideoUrl() != null) {
            if (!ads.get(i).getVideoUrl().equals("")) {
                clickVideoButton(holder, ads.get(i).getVideoUrl(), i);
            } else if (ads.get(i).getVideoUrlHigh() != null) {
                if (!ads.get(i).getVideoUrlHigh().equals("")) {
                    clickVideoButton(holder, ads.get(i).getVideoUrlHigh(), i);
                } else if (ads.get(i).getVideoUrl30Sec() != null) {
                    if (!ads.get(i).getVideoUrl30Sec().equals("")) {
                        clickVideoButton(holder, ads.get(i).getVideoUrl30Sec(), i);
                    } else if (ads.get(i).getVideoUrlHigh30Sec() != null) {
                        if (!ads.get(i).getVideoUrlHigh().equals("")) {
                            clickVideoButton(holder, ads.get(i).getVideoUrlHigh(), i);
                        }
                    }
                }
            }

        }
    }

    private void clickVideoButton(ViewHolder holder, final String videoUrl, final int i){
        holder.playVideoButton.setEnabled(true);
        holder.playVideoButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, VideoActivity.class);
                        intent.putExtra("videoUrl", videoUrl);
                        intent.putExtra("ad", ads.get(i));
                        context.startActivity(intent);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return ads.size();
    }


    public static class DownloadImage extends AsyncTask<String, Integer, Drawable> {

        @SuppressLint("StaticFieldLeak")
        ImageView mImageView;

        private DownloadImage(ImageView imageView) {
            mImageView = imageView;
        }

        @Override
        protected Drawable doInBackground(String... arg0) {
            return downloadImage(arg0[0]);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        protected void onPostExecute(Drawable image)
        {
            mImageView.setBackground(image);
        }

        private Drawable downloadImage(String _url)
        {
            URL url;
            InputStream in;
            BufferedInputStream buf;

            try {
                url = new URL(_url);
                in = url.openStream();

                buf = new BufferedInputStream(in);

                Bitmap bMap = BitmapFactory.decodeStream(buf);
                if (in != null) {
                    in.close();
                }
                if (buf != null) {
                    buf.close();
                }

                return new BitmapDrawable(bMap);

            } catch (Exception e) {
                Log.e("Error reading file", e.toString());
            }
            return null;
        }
    }

    public void setAppnextCallbacks(AppnextCallbacks appnextCallbacks) {
        this.appnextCallbacks = appnextCallbacks;
    }
}
