package megaapp.qa.appnext.com.megatestapp.configuration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import megaapp.qa.appnext.com.megatestapp.nativeadssdknew.NativeAdsActivityNew;
import megaapp.qa.appnext.com.megatestapp.R;

public class NativeAdsSdkNewConfig extends AppCompatActivity implements View.OnClickListener{

    private EditText postBackText;
    private EditText categories;
    private EditText videoMinLength;
    private EditText videoMaxLength;
    private String creativeTypeSelected;
    private String videoLengthSelected;
    private String videoQualitySelected;
    private String cachingPolicySelected;
    private Switch autoPlaySwitch;
    private Switch muteSwitch;
    private Switch clickEnabledSwitch;
    private SharedPreferences spSetters;
    private int ppPosition;
    private int ppColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_ads_sdk_new_config);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        spSetters = getSharedPreferences("setters", MODE_PRIVATE);
        Button okBtn = findViewById(R.id.okBtn);
        okBtn.setOnClickListener(this);
        postBackText = findViewById(R.id.postBackText);
        categories = findViewById(R.id.categoriesText);
        videoMinLength = findViewById(R.id.minVideoLengthET);
        videoMaxLength = findViewById(R.id.maxVideoLengthET);
        muteSwitch = findViewById(R.id.muteSwitch);
        muteSwitch.setChecked(false);
        muteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            }
        });

        autoPlaySwitch = findViewById(R.id.autoPlaySwitch);
        autoPlaySwitch.setChecked(true);
        autoPlaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            boolean selectionB;
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectionB = autoPlaySwitch.isChecked();
            }
        });

        clickEnabledSwitch = findViewById(R.id.clickEnabledSwitch);
        clickEnabledSwitch.setChecked(true);
        clickEnabledSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            boolean clickEnabledSelection;
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                clickEnabledSelection = clickEnabledSwitch.isChecked();
            }
        });
        Spinner creativeTypeSpinner = findViewById(R.id.creativeTypeSpinner);
        creativeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                creativeTypeSelected = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Spinner videoLengthSpinner = findViewById(R.id.videoLengthSpinner);
        videoLengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                videoLengthSelected = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        Spinner videoQualitySpinner = findViewById(R.id.videoQualitySpinner);
        videoQualitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                videoQualitySelected = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        Spinner cachingPolicySpinner = findViewById(R.id.cachingPolicySpinner);
        cachingPolicySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                cachingPolicySelected = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Spinner privacyPolicyColorSpinner = findViewById(R.id.privacyPolicyColorSpinner);
        privacyPolicyColorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            String privacyPolicyColor;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                privacyPolicyColor = parent.getItemAtPosition(position).toString();

                switch (privacyPolicyColor){
                    case("Dark"):
                        ppColor = 1;
                        break;
                    case("Light"):
                        ppColor = 0;
                        break;
                    case("PP Color"):
                        ppColor = 0;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner privacyPolicyPositionSpinner = findViewById(R.id.privacyPolicyPositionSpinner);
        privacyPolicyPositionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            String privacyPolicyPosition;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                privacyPolicyPosition = parent.getItemAtPosition(position).toString();
                switch (privacyPolicyPosition){
                    case("Upper Right"):
                        ppPosition = 1;
                        break;
                    case("Upper Left"):
                        ppPosition = 0;
                        break;
                    case("Bottom Right"):
                        ppPosition = 3;
                        break;
                    case("Bottom Left"):
                        ppPosition = 2;
                        break;
                    case("PP Position"):
                        ppPosition = 1;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        //spSetters = getSharedPreferences("setters", MODE_PRIVATE);

        spSetters.edit()
                .putString("category", categories.getText().toString())
                .putString("postBack", postBackText.getText().toString())
                .putInt("videoMin", videoMinLength.getInputType())
                .putInt("videoMax", videoMaxLength.getInputType())
                .putBoolean("mute", muteSwitch.isChecked())
                .putBoolean("autoPlay", autoPlaySwitch.isChecked())
                .putBoolean("clickEnabled", clickEnabledSwitch.isChecked())
                .putString("creativeType", creativeTypeSelected)
                .putString("videoQuality", videoQualitySelected)
                .putString("videoLength", videoLengthSelected)
                .putString("cachingPolicy", cachingPolicySelected)
                .putInt("privacyPosition", ppPosition)
                .putInt("privacyColor", ppColor)
                .apply();
        System.out.println("#@#@ " + spSetters.getBoolean("autoPlay", true));
        startActivity(new Intent(NativeAdsSdkNewConfig.this, NativeAdsActivityNew.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Click 'Save' Button", Toast.LENGTH_SHORT).show();    }
}
