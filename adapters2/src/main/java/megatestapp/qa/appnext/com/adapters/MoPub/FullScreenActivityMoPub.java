package megatestapp.qa.appnext.com.adapters.mopub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.ads.fullscreen.Video;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import java.util.HashMap;
import java.util.Map;
import megatestapp.qa.appnext.com.adapters.R;
import megatestapp.qa.appnext.com.adapters.utilities.LogToastUtil;

public class FullScreenActivityMoPub extends AppCompatActivity implements View.OnClickListener{
    private Button showDefaultFullScreen;
    private Button showCustomFullScreen;
    private MoPubInterstitial defaultEventFullScreen;
    private MoPubInterstitial customEventFullScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_mopub);

        Button loadDefaultFullScreen = findViewById(R.id.loadDefaultFullScreen);
        loadDefaultFullScreen.setOnClickListener(this);

        showDefaultFullScreen = findViewById(R.id.showDefaultFullScreen);
        showDefaultFullScreen.setOnClickListener(this);
        showDefaultFullScreen.setEnabled(false);


        Button loadCustomFullScreen = findViewById(R.id.loadCustomFullScreen);
        loadCustomFullScreen.setOnClickListener(this);

        showCustomFullScreen = findViewById(R.id.showCustomFullScreen);
        showCustomFullScreen.setOnClickListener(this);
        showCustomFullScreen.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadDefaultFullScreen) {
            if (defaultEventFullScreen != null) {
                defaultEventFullScreen.destroy();
            }
            defaultEventFullScreen = new MoPubInterstitial(this, "cf0a90e7c0e64c2f9fba9cdea58c6d97");
            callBacks(defaultEventFullScreen, "default");
            defaultEventFullScreen.load();

        } else if (i == R.id.showDefaultFullScreen) {
            defaultEventFullScreen.show();


        } else if (i == R.id.loadCustomFullScreen) {
            FullscreenConfig fullScreenCustomConfig = new FullscreenConfig();
            fullScreenCustomConfig.setCategories("Action");
            fullScreenCustomConfig.setPostback("YOUR_POSTBACK_HERE");
            fullScreenCustomConfig.setBackButtonCanClose(false);
            fullScreenCustomConfig.setMute(false);
            fullScreenCustomConfig.setVideoLength(Video.VIDEO_LENGTH_LONG);
            fullScreenCustomConfig.setOrientation(Video.ORIENTATION_LANDSCAPE);
            fullScreenCustomConfig.setShowClose(true, 7500);
            fullScreenCustomConfig.setShowCta(true);
            fullScreenCustomConfig.setLanguage("KO");
            customEventFullScreen = new MoPubInterstitial(this, "cf0a90e7c0e64c2f9fba9cdea58c6d97");

            Map<String, Object> fullScreenExtras = new HashMap<>();
            fullScreenExtras.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, fullScreenCustomConfig);
            customEventFullScreen.setLocalExtras(fullScreenExtras);
            callBacks(customEventFullScreen, "custom");
            customEventFullScreen.load();


        } else if (i == R.id.showCustomFullScreen) {
            customEventFullScreen.show();


        }
    }

    public void callBacks(MoPubInterstitial fullScreenEvent, final String eventType ){
        fullScreenEvent.setInterstitialAdListener(new MoPubInterstitial.InterstitialAdListener() {
            @Override
            public void onInterstitialLoaded(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultFullScreen.setEnabled(true);
                }else{
                    showCustomFullScreen.setEnabled(true);
                }
                LogToastUtil.logToast(FullScreenActivityMoPub.this, "FullScreen MoPub - Loaded");
            }

            @Override
            public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
                LogToastUtil.logToast(FullScreenActivityMoPub.this, "FullScreen MoPub - Failed");            }

            @Override
            public void onInterstitialShown(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultFullScreen.setEnabled(false);
                }else{
                    showCustomFullScreen.setEnabled(false);
                }
                LogToastUtil.logToast(FullScreenActivityMoPub.this, "FullScreen MoPub - Shown");
            }

            @Override
            public void onInterstitialClicked(MoPubInterstitial interstitial) {
                LogToastUtil.logToast(FullScreenActivityMoPub.this, "FullScreen MoPub - Clicked");

            }

            @Override
            public void onInterstitialDismissed(MoPubInterstitial interstitial) {
                LogToastUtil.logToast(FullScreenActivityMoPub.this, "FullScreen MoPub - Dismissed");
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (defaultEventFullScreen != null) {
            defaultEventFullScreen.destroy();
        }
        if (customEventFullScreen != null) {
            customEventFullScreen.destroy();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (defaultEventFullScreen != null) {
//            defaultEventFullScreen.destroy();
//        }
//        if (customEventFullScreen != null) {
//            customEventFullScreen.destroy();
//        }
        startActivity(new Intent(FullScreenActivityMoPub.this, MoPubMainActivity.class));
        finish();
    }
}
