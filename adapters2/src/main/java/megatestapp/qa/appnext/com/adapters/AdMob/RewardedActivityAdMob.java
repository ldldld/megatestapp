package megatestapp.qa.appnext.com.adapters.admob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedServerSidePostback;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobRewardedVideoAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import megatestapp.qa.appnext.com.adapters.R;


public class RewardedActivityAdMob extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewarded_native_sdk);

        Button loadDefaultRewarded = findViewById(R.id.loadRewardedDefaultAdMob);
        loadDefaultRewarded.setOnClickListener(this);

        Button loadCustomRewarded = findViewById(R.id.loadRewardedCustomAdMob);
        loadCustomRewarded.setOnClickListener(this);

        Button rewardedSetters = findViewById(R.id.rewardedSettersAdMob);
        rewardedSetters.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadRewardedDefaultAdMob) {
            RewardedVideoAd defaultEventRewarded = MobileAds.getRewardedVideoAdInstance(this);
            Bundle defaultEventExtrasRewarded = new Bundle();
            defaultEventRewarded.loadAd("ca-app-pub-4981994338959649/6878021014",
                    new AdRequest.Builder().
                            addNetworkExtrasBundle(AppnextAdMobRewardedVideoAdapter.class,
                                    defaultEventExtrasRewarded).build());
            callBacks(defaultEventRewarded);


        } else if (i == R.id.loadRewardedCustomAdMob) {
            RewardedVideoAd customEventRewarded = MobileAds.getRewardedVideoAdInstance(this);
            RewardedConfig rewardedConfig = new RewardedConfig();
            rewardedConfig.setPostback("Your_Postback_Params");
            rewardedConfig.setCategories("Categories");
            rewardedConfig.setMute(false);
            //rewardedConfig.setVideoLength(FullScreenVideo.VIDEO_LENGTH_DEFAULT);
            rewardedConfig.setOrientation(FullScreenVideo.ORIENTATION_LANDSCAPE);
            rewardedConfig.setMinVideoLength(4);
            rewardedConfig.setMinVideoLength(8);
            rewardedConfig.setMode("multi");
            rewardedConfig.setLanguage("KO");


            RewardedServerSidePostback rewardedServerSidePostback = new RewardedServerSidePostback();
            rewardedServerSidePostback.setRewardsTransactionId("aa");
            rewardedServerSidePostback.setRewardsUserId("bb");
            rewardedServerSidePostback.setRewardsCustomParameter("cc");
            rewardedServerSidePostback.setRewardsRewardTypeCurrency("dd");
            rewardedServerSidePostback.setRewardsAmountRewarded("eee");

            Bundle customEventExtrasRewarded = new Bundle();
            customEventExtrasRewarded.putSerializable(AppnextAdMobCustomEvent
                    .AppnextConfigurationExtraKey, rewardedConfig);
            customEventExtrasRewarded.putSerializable(AppnextAdMobCustomEvent
                    .AppnextRewardPostbackExtraKey, rewardedServerSidePostback);
            customEventRewarded.loadAd("ca-app-pub-4981994338959649/6878021014",
                    new AdRequest.Builder()
                            .addNetworkExtrasBundle(AppnextAdMobRewardedVideoAdapter.class,
                                    customEventExtrasRewarded).build());
            callBacks(customEventRewarded);
        }
    }

    public void callBacks(final RewardedVideoAd rewardedEvent){
        rewardedEvent.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                Toast.makeText(RewardedActivityAdMob.this, "Rewarded Ad Loaded", Toast.LENGTH_SHORT).show();
                rewardedEvent.show();

            }

            @Override
            public void onRewardedVideoAdOpened() {
                Toast.makeText(RewardedActivityAdMob.this, "Rewarded Ad Opened", Toast.LENGTH_SHORT).show();
                // mCustomEventRewardedButton.setEnabled(false);
            }

            @Override
            public void onRewardedVideoStarted() {

            }

            @Override
            public void onRewardedVideoAdClosed() {
                Toast.makeText(RewardedActivityAdMob.this, "Rewarded Ad Closed", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
//                Toast.makeText(BannersPage.this,"Rewarded Ad Closed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                Toast.makeText(RewardedActivityAdMob.this,
                        "Error loading custom event rewarded, code ",//errorCod
                        Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewardedVideoCompleted() {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RewardedActivityAdMob.this, AdMobMainActivity.class));
        finish();
    }
}
