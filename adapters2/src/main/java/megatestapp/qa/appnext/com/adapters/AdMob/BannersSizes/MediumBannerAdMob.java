package megatestapp.qa.appnext.com.adapters.admob.bannerssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.admob.banners.AppnextAdMobBannerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


import megatestapp.qa.appnext.com.adapters.admob.BannersAdMob;
import megatestapp.qa.appnext.com.adapters.R;

import static com.appnext.banners.BannerAdRequest.TYPE_VIDEO;
import static com.appnext.banners.BannerAdRequest.VIDEO_LENGTH_LONG;

public class MediumBannerAdMob extends AppCompatActivity implements View.OnClickListener{
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medium_banner_ad_mob);

        Button loadDefaultMediumBanner = findViewById(R.id.loadDefaultMediumBanner);
        loadDefaultMediumBanner.setOnClickListener(this);

        Button loadCustomMediumBanner = findViewById(R.id.loadCustomMediumBanner);
        loadCustomMediumBanner.setOnClickListener(this);

        Button mediumBannerSetters = findViewById(R.id.mediumBannerSetters);
        mediumBannerSetters.setOnClickListener(this);
        mAdView = findViewById(R.id.adViewMedium);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadDefaultMediumBanner) {
            if (mAdView != null) {
                mAdView.destroy();
            }
            BannerAdRequest configMediumDef = new BannerAdRequest();
            Bundle extras = new Bundle();
            extras.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configMediumDef);
            extras.putString(AppnextAdMobBannerAdapter.AppNextBannerLanguageKey, "KO");

            AdRequest adRequestDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extras).build();
            mAdView.loadAd(adRequestDefault);
            callBacks();

        } else if (i == R.id.loadCustomMediumBanner) {
            if (mAdView != null) {
                mAdView.destroy();
            }
            BannerAdRequest configMediumCus = new BannerAdRequest();
            Bundle extrasSmall = new Bundle();
            configMediumCus.setCategories("Games, Adventure")
                    .setPostback("AdMob Installed")

//Relevant for MEDIUM_RECTANGLE size only
                    .setCreativeType(TYPE_VIDEO)
                    .setAutoPlay(true)
                    .setMute(true)
                    .setVideoLength(VIDEO_LENGTH_LONG)
                    .setClickEnabled(true)
                    .setVidMax(5)
                    .setVidMin(25);


            extrasSmall.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configMediumCus);
            AdRequest adRequestCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extrasSmall).build();

            mAdView.loadAd(adRequestCustom);
            callBacks();
        }else if (i == R.id.mediumBannerSetters){
            SharedPreferences spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);
            spSettersAdMob.edit().putString("whoAmI", "medium").apply();
            startActivity(new Intent(MediumBannerAdMob.this,
                    AdMobBannersWithSetters.class));


        }

    }
    public void callBacks() {
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Toast.makeText(MediumBannerAdMob.this, "Banner Loaded", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.

                Toast.makeText(MediumBannerAdMob.this, "Banner Load Error", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Toast.makeText(MediumBannerAdMob.this, "Banner Opened", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Toast.makeText(MediumBannerAdMob.this, "Banner Left App", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                Toast.makeText(MediumBannerAdMob.this, "Banner Closed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mAdView != null) {
            mAdView.destroy();
        }
        startActivity(new Intent(MediumBannerAdMob.this, BannersAdMob.class));
        finish();
    }
}
