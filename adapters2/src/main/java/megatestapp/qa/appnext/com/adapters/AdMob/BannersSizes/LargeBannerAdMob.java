package megatestapp.qa.appnext.com.adapters.admob.bannerssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.admob.banners.AppnextAdMobBannerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import megatestapp.qa.appnext.com.adapters.admob.BannersAdMob;
import megatestapp.qa.appnext.com.adapters.R;

public class LargeBannerAdMob extends AppCompatActivity implements View.OnClickListener{
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_banner_ad_mob);

        Button loadDefaultLargeBanner = findViewById(R.id.loadDefaultLargeBanner);
        loadDefaultLargeBanner.setOnClickListener(this);
        Button loadCustomLargeBanner = findViewById(R.id.loadCustomLargeBanner);
        loadCustomLargeBanner.setOnClickListener(this);
        Button largeBannerSetters = findViewById(R.id.settersLarge);
        largeBannerSetters.setOnClickListener(this);
        mAdView = findViewById(R.id.adViewLarge);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadDefaultLargeBanner) {
            if (mAdView != null) {
                mAdView.destroy();
            }
            BannerAdRequest configLargeDef = new BannerAdRequest();
            Bundle extras = new Bundle();
            extras.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configLargeDef);
            extras.putString(AppnextAdMobBannerAdapter.AppNextBannerLanguageKey, "KO");
            AdRequest adRequestDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extras).build();

            callBacks();
            mAdView.loadAd(adRequestDefault);

        } else if (i == R.id.loadCustomLargeBanner) {
            if (mAdView != null) {
                mAdView.destroy();
            }
            BannerAdRequest configLargeCus = new BannerAdRequest();
            Bundle extrasSmall = new Bundle();
            configLargeCus.setCategories("Games, Adventure")
                    .setPostback("AdMob Installed");
            extrasSmall.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configLargeCus);
            AdRequest adRequestCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extrasSmall).build();
            callBacks();
            mAdView.loadAd(adRequestCustom);


        } else if (i == R.id.settersLarge){
            SharedPreferences spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);
            spSettersAdMob.edit().putString("whoAmI", "large").apply();
                startActivity(new Intent(LargeBannerAdMob.this,
                        AdMobBannersWithSetters.class));


        }
    }
    public void callBacks() {
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Toast.makeText(LargeBannerAdMob.this, "Banner Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(LargeBannerAdMob.this, "Banner Load Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Toast.makeText(LargeBannerAdMob.this, "Banner Opened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(LargeBannerAdMob.this, "Banner Left App", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(LargeBannerAdMob.this, "Banner Closed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mAdView != null) {
            mAdView.destroy();
        }
        startActivity(new Intent(LargeBannerAdMob.this, BannersAdMob.class));
        finish();
    }
}
