package megatestapp.qa.appnext.com.adapters.mopub;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedServerSidePostback;
import com.appnext.ads.fullscreen.Video;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.mopub.common.MoPubReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedVideoListener;
import com.mopub.mobileads.MoPubRewardedVideoManager;
import com.mopub.mobileads.MoPubRewardedVideos;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import megatestapp.qa.appnext.com.adapters.R;
import megatestapp.qa.appnext.com.adapters.utilities.LogToastUtil;

public class RewardedMoPub extends AppCompatActivity implements View.OnClickListener{
    private Button showDefaultRewarded;
    private Button showCustomtRewarded;
    private static MoPubRewardedVideos moPubVideoDefault = null;
   // private MoPubRewardedVideos moPubVideoCustom = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewarded_mo_pub);

        Button loadDefaultRewarded = findViewById(R.id.loadDefaultRewarded);
        loadDefaultRewarded.setOnClickListener(this);

        showDefaultRewarded = findViewById(R.id.showDefaultRewarded);
        showDefaultRewarded.setOnClickListener(this);
        showDefaultRewarded.setEnabled(false);

        Button loadCustomRewarded = findViewById(R.id.loadCustomRewarded);
        loadCustomRewarded.setOnClickListener(this);

        showCustomtRewarded = findViewById(R.id.showCustomRewarded);
        showCustomtRewarded.setOnClickListener(this);
        showCustomtRewarded.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadDefaultRewarded) {

//            MoPubRewardedVideos.initializeRewardedVideo(this, new AppnextMoPubRewardedVideo.
//                    AppnextMediationSettings.Builder()
//                    .withRewardsAmountRewarded("asdasd").build());
            MoPubRewardedVideoManager.updateActivity(this);
            MoPubRewardedVideos.loadRewardedVideo("b0dc3f27b8d64231b5d8e29a59a93a35"); //appnext pid = e5208600-232f-47b6-bf16-bf8727fae5f0

            callBacks(moPubVideoDefault, "default");

        } else if (i == R.id.showDefaultRewarded) {
            MoPubRewardedVideos.showRewardedVideo("b0dc3f27b8d64231b5d8e29a59a93a35");


        } else if (i == R.id.loadCustomRewarded) {
//            MoPubRewardedVideos.initializeRewardedVideo(this, new AppnextMoPubRewardedVideo.
//                    AppnextMediationSettings.Builder()
//                    .withRewardsAmountRewarded("asdasd").build());

            RewardedConfig rewardedConfig = new RewardedConfig();
            rewardedConfig.setCategories("Productivity");
            rewardedConfig.setPostback("QA Test Rewarded Mopub");
            rewardedConfig.setMute(true);
            //rewardedConfig.setVideoLength("15");
            rewardedConfig.setOrientation(Video.ORIENTATION_LANDSCAPE);
            // rewardedConfig.setMaxVideoLength(40);
            Video.setCacheVideo(true);
            rewardedConfig.setMode("multi");
            rewardedConfig.setMultiTimerLength(15);
            rewardedConfig.setShowCta(true);
            rewardedConfig.setLanguage("KO");
            Video.setCacheVideo(false);
            RewardedServerSidePostback postbackParams = new RewardedServerSidePostback("aTran", "bUser", "cCurr", "dAmou", "eParam");
            Map<String, Object> extrasRewarded = new HashMap();
            extrasRewarded.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, rewardedConfig);
            extrasRewarded.put(AppnextMoPubCustomEvent.AppnextRewardPostbackExtraKey, postbackParams);
           // mCustomisedCustomEventRewarded.setLocalExtras(extrasRewarded);
  //  adUnitId, requestParameters, mediationSettings
            MoPubRewardedVideoManager.updateActivity(this);
            callBacks(moPubVideoDefault, "custom");
            MoPubRewardedVideos.loadRewardedVideo("a7c53e90ed654bada3908b6f71250593"); //appnext pid = e5208600-232f-47b6-bf16-bf8727fae5f0

        } else if (i == R.id.showCustomRewarded) {
            callBacks(moPubVideoDefault, "custom");
            MoPubRewardedVideos.showRewardedVideo("a7c53e90ed654bada3908b6f71250593");
        }
    }

//
//    Map<String, Object> extras = new HashMap<>();
////                extras.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, config);
////                extras.put(AppnextMoPubCustomEvent.AppnextRewardPostbackExtraKey, postbackParams);
////                mCustomisedCustomEventRewarded.setLocalExtras(extras);
//                MoPubRewardedVideos.initializeRewardedVideo(MoPubMainActivity.this,new AppnextMoPubRewardedVideo.AppnextMediationSettings.Builder()
////                        .withRewardsCustomParameter("AppnextMute")
//            .build());
//    createCustomisedRewarded();
//                MoPubRewardedVideos.loadRewardedVideo(getResources().getString(R.string.mopub_customevent_rewarded_ad_unit_id));//,new MoPubRewardedVideoManager.RequestParameters());
//}
//        });
//
//
//                Add CommentCollapse 

    public void callBacks(MoPubRewardedVideos moPubVideoEvent, final String eventType){

        MoPubRewardedVideos.setRewardedVideoListener(new MoPubRewardedVideoListener() {
            @Override
            public void onRewardedVideoLoadSuccess(@NonNull String customevent_rewarded_ad_unit_id) {
                if(eventType.equals("default")){
                    showDefaultRewarded.setEnabled(true);
                }else{
                    showCustomtRewarded.setEnabled(true);
                }
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Loaded");
            }

            @Override
            public void onRewardedVideoLoadFailure(@NonNull String customevent_rewarded_ad_unit_id, @NonNull MoPubErrorCode errorCode) {
                if(eventType.equals("default")){
                    showDefaultRewarded.setEnabled(false);
                }else{
                    showCustomtRewarded.setEnabled(false);
                }
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Failure");
            }

            @Override
            public void onRewardedVideoStarted(@NonNull String customevent_rewarded_ad_unit_id) {
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Video Started");            }

            @Override
            public void onRewardedVideoPlaybackError(@NonNull String customevent_rewarded_ad_unit_id, @NonNull MoPubErrorCode errorCode) {
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Playback Error");            }

            @Override
            public void onRewardedVideoClicked(@NonNull String adUnitId) {
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Video Clicked");
            }

            @Override
            public void onRewardedVideoClosed(@NonNull String customevent_rewarded_ad_unit_id) {
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Video Closed");
            }

            @Override
            public void onRewardedVideoCompleted(@NonNull Set<String> customevent_rewarded_ad_unit_id, @NonNull MoPubReward reward) {
                if(eventType.equals("default")){
                    showDefaultRewarded.setEnabled(false);
                }else{
                    showCustomtRewarded.setEnabled(false);
                }
                LogToastUtil.logToast(RewardedMoPub.this, "Rewarded MoPub - Video Completed");
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RewardedMoPub.this, MoPubMainActivity.class));
        finish();
    }
}
