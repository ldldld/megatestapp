package megatestapp.qa.appnext.com.adapters.admob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEventFullScreenVideo;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.wang.avi.AVLoadingIndicatorView;

import megatestapp.qa.appnext.com.adapters.R;


public class FullScreenActivityAdMob extends AppCompatActivity implements View.OnClickListener{
    private AVLoadingIndicatorView avi;

    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_ad_mob);
        Button loadDefaultFs = findViewById(R.id.loadFsDefaultAdMob);
        loadDefaultFs.setOnClickListener(this);

        Button loadCustomFs = findViewById(R.id.loadFsCustomAdMob);
        loadCustomFs.setOnClickListener(this);

        Button fsSetters = findViewById(R.id.fsSettersAdMob);
        fsSetters.setOnClickListener(this);
        avi= findViewById(R.id.avi);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        hideClick(avi);


    }
    void startAnim(){
        avi.show();
        // or avi.smoothToShow();
    }
    public void hideClick(View view) {
        avi.hide();
        // or avi.smoothToHide();
    }
    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadFsDefaultAdMob) {
            InterstitialAd defaultFullScreen = new InterstitialAd(this);
            defaultFullScreen.setAdUnitId("ca-app-pub-4981994338959649/3057401011");

            FullscreenConfig fullscreenDef = new FullscreenConfig();
            fullscreenDef.setLanguage("KO");
            Bundle defaultEventExtrasFS = new Bundle();
            defaultEventExtrasFS.putSerializable(AppnextAdMobCustomEvent.AppnextConfigurationExtraKey, fullscreenDef);
            AdRequest adRequestFSDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobCustomEventFullScreenVideo.class, defaultEventExtrasFS).build();
            defaultFullScreen.loadAd(adRequestFSDefault);
            progressBar.setVisibility(View.VISIBLE);
            startAnim();
            callBacks(defaultFullScreen);

        } else if (i == R.id.loadFsCustomAdMob) {
            InterstitialAd customFullScreen = new InterstitialAd(this);
            customFullScreen.setAdUnitId("ca-app-pub-4981994338959649/3057401011");
            FullscreenConfig fullscreenConfig = new FullscreenConfig();
            fullscreenConfig.setPostback("Admob Fullscreen");
            fullscreenConfig.setCategories("Shopping");
            fullscreenConfig.setMute(false);
            fullscreenConfig.setBackButtonCanClose(true);
            fullscreenConfig.setVideoLength(FullScreenVideo.VIDEO_LENGTH_LONG);
            fullscreenConfig.setOrientation(FullScreenVideo.ORIENTATION_PORTRAIT);
            fullscreenConfig.setMinVideoLength(4);
            fullscreenConfig.setMaxVideoLength(4);
            fullscreenConfig.setShowClose(true, 6000);
            Bundle customEventExtrasFS = new Bundle();
            customEventExtrasFS.putSerializable(AppnextAdMobCustomEvent.AppnextConfigurationExtraKey, fullscreenConfig);
            AdRequest adRequestFSCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobCustomEventFullScreenVideo.class, customEventExtrasFS).build();
            customFullScreen.loadAd(adRequestFSCustom);
            progressBar.setVisibility(View.VISIBLE);
            callBacks(customFullScreen);
        }
    }
    public void callBacks(final InterstitialAd fullScreenEvent){
        fullScreenEvent.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(FullScreenActivityAdMob.this,
                        "Error loading customized custom event fullscreen, code " + errorCode,
                        Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
                hideClick(avi);
                // mCustomisedCustomEventFullScreenButton.setEnabled(true);
            }

            @Override
            public void onAdLoaded() {
                Toast.makeText(FullScreenActivityAdMob.this, "Fullscreen Ad Loaded", Toast.LENGTH_SHORT).show();
                fullScreenEvent.show();
                progressBar.setVisibility(View.INVISIBLE);
                hideClick(avi);
                //mCustomisedCustomEventFullScreenButton.setEnabled(true);
            }

            @Override
            public void onAdOpened() {
                Toast.makeText(FullScreenActivityAdMob.this, "Fullscreen Ad Opened", Toast.LENGTH_SHORT).show();
                //mCustomisedCustomEventFullScreenButton.setEnabled(false);
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(FullScreenActivityAdMob.this, "Fullscreen Ad Closed", Toast.LENGTH_SHORT).show();
                // configAndLoadCustomisedFullScreen();
            }

            @Override
            public void onAdImpression(){
                Toast.makeText(FullScreenActivityAdMob.this, "Fullscreen Impression", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdClicked(){
                Toast.makeText(FullScreenActivityAdMob.this, "Fullscreen Clicked", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdLeftApplication (){
                Toast.makeText(FullScreenActivityAdMob.this, "Fullscreen Left App", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FullScreenActivityAdMob.this, AdMobMainActivity.class));
        finish();
    }
}
