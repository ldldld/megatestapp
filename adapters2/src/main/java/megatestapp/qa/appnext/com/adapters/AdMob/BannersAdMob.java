package megatestapp.qa.appnext.com.adapters.admob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import megatestapp.qa.appnext.com.adapters.admob.bannerssizes.LargeBannerAdMob;
import megatestapp.qa.appnext.com.adapters.admob.bannerssizes.MediumBannerAdMob;
import megatestapp.qa.appnext.com.adapters.admob.bannerssizes.SmallBannerAdMob;

import megatestapp.qa.appnext.com.adapters.admob.bannerssizes.SmallBannerAdMobMulti;
import megatestapp.qa.appnext.com.adapters.admob.bannerssizes.SmallBannerAdMobMulti2;
import megatestapp.qa.appnext.com.adapters.R;


public class BannersAdMob extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_ad_mob);

        Button bannerBtn = findViewById(R.id.smallBannerAdMob);
        bannerBtn.setOnClickListener(this);
        Button bannerBtnMulti1 = findViewById(R.id.smallBannerAdMobMulti1);
        bannerBtnMulti1.setOnClickListener(this);
        Button bannerBtnMulti2 = findViewById(R.id.smallBannerAdMobMulti2);
        bannerBtnMulti2.setOnClickListener(this);
        Button mediumBtn = findViewById(R.id.mediumBannerAdMob);
        mediumBtn.setOnClickListener(this);
        Button largeBtn = findViewById(R.id.largeBannerAdMob);
        largeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.smallBannerAdMob) {
            startActivity(new Intent(BannersAdMob.this, SmallBannerAdMob.class));
            finish();

        } else if (i == R.id.mediumBannerAdMob) {
            startActivity(new Intent(BannersAdMob.this, MediumBannerAdMob.class));
            finish();

        } else if (i == R.id.largeBannerAdMob) {
            startActivity(new Intent(BannersAdMob.this, LargeBannerAdMob.class));
            finish();

        }else if (i == R.id.smallBannerAdMobMulti1) {
            startActivity(new Intent(BannersAdMob.this, SmallBannerAdMobMulti.class));
            finish();

        }else if (i == R.id.smallBannerAdMobMulti2) {
            startActivity(new Intent(BannersAdMob.this, SmallBannerAdMobMulti2.class));
            finish();

        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(BannersAdMob.this, AdMobMainActivity.class));
        finish();
    }
}
