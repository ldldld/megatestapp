package megatestapp.qa.appnext.com.adapters.admob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.core.Ad;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEventInterstitial;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import megatestapp.qa.appnext.com.adapters.R;


public class InterstitialAdMob extends AppCompatActivity implements View.OnClickListener{
    private InterstitialAd customInterstitial;
    private InterstitialAd defaultInterstitial;
    private Button showCustomInterstitial;
    private Button showDefaultInterstitial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial_ad_mob);
        Button loadCustomInterstitial = findViewById(R.id.loadCustomInterstitialAdMob);
        loadCustomInterstitial.setOnClickListener(this);

        showCustomInterstitial = findViewById(R.id.isCustomInterstitialLoadedAdMob);
        showCustomInterstitial.setOnClickListener(this);
        showCustomInterstitial.setEnabled(false);

        Button loadDefaultInterstitial = findViewById(R.id.loadDefaultInterstitialAdMob);
        loadDefaultInterstitial.setOnClickListener(this);

        showDefaultInterstitial = findViewById(R.id.isDefaultInterstitialLoadedAdMob);
        showDefaultInterstitial.setOnClickListener(this);
        showDefaultInterstitial.setEnabled(false);
        //Appnext.init(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadCustomInterstitialAdMob) {
            InterstitialConfig config = new InterstitialConfig();
            config.setButtonColor("#6AB344");
            config.setPostback("Your_Postback_Params");
            config.setCategories("Categories");
            config.setSkipText("Skip");
            config.setMute(false);
            config.setAutoPlay(true);
            config.setCreativeType(Interstitial.TYPE_MANAGED);
            config.setOrientation(Ad.ORIENTATION_DEFAULT);
            config.setLanguage("KO");
            Bundle customEventExtras = new Bundle();
            customEventExtras.putSerializable(AppnextAdMobCustomEvent.AppnextConfigurationExtraKey, config);
            AdRequest adRequestInterstitial = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobCustomEventInterstitial.class, customEventExtras).build();
            customInterstitial = new InterstitialAd(this);
            customInterstitial.setAdUnitId("ca-app-pub-4981994338959649/8964333816");
            callBack(customInterstitial, "custom");
            customInterstitial.loadAd(adRequestInterstitial);

        } else if (i == R.id.isCustomInterstitialLoadedAdMob) {
            customInterstitial.show();

        } else if (i == R.id.loadDefaultInterstitialAdMob) {
            defaultInterstitial = new InterstitialAd(this);
            defaultInterstitial.setAdUnitId("ca-app-pub-4981994338959649/8964333816");
            callBack(defaultInterstitial, "default");
            defaultInterstitial.loadAd(new AdRequest.Builder().build());

        } else if (i == R.id.isDefaultInterstitialLoadedAdMob) {
            defaultInterstitial.show();

        }
    }
    public void callBack(InterstitialAd interstitialAd, final String interstitialType){
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(InterstitialAdMob.this,
                        "Error loading customized custom event interstitial, code " + errorCode,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                if(interstitialType.equals("custom")){
                    showCustomInterstitial.setEnabled(true);
                }
                if(interstitialType.equals("default")){
                    showDefaultInterstitial.setEnabled(true);
                }

                Toast.makeText(InterstitialAdMob.this, "Interstitial Ad Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                Toast.makeText(InterstitialAdMob.this, "Interstitial Ad Opened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(InterstitialAdMob.this, "Interstitial Ad Closed", Toast.LENGTH_SHORT).show();
                if(interstitialType.equals("custom")){
                    showCustomInterstitial.setEnabled(false);
                }
                if(interstitialType.equals("default")){
                    showDefaultInterstitial.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(InterstitialAdMob.this, AdMobMainActivity.class));
        finish();
    }
}
