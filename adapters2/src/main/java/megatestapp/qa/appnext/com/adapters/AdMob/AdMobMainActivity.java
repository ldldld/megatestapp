package megatestapp.qa.appnext.com.adapters.admob;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.MobileAds;

import megatestapp.qa.appnext.com.adapters.R;


public class AdMobMainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_mob_main);
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");
        Button bannersBtnAdMob = findViewById(R.id.bannersAdMobPage);
        bannersBtnAdMob.setOnClickListener(this);

        Button interstitialBtnAdMob = findViewById(R.id.interstitialAdMobPage);
        interstitialBtnAdMob.setOnClickListener(this);

        Button fullScreenBtnAdMob = findViewById(R.id.fullScreenAdMobPage);
        fullScreenBtnAdMob.setOnClickListener(this);

        Button rewardedBtnAdMob = findViewById(R.id.rewardedAdMobPage);
        rewardedBtnAdMob.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.bannersAdMobPage) {
            startActivity(new Intent(AdMobMainActivity.this, BannersAdMob.class));
            finish();

        } else if (i == R.id.interstitialAdMobPage) {
            startActivity(new Intent(AdMobMainActivity.this, InterstitialAdMob.class));
            finish();

        } else if (i == R.id.fullScreenAdMobPage) {
            startActivity(new Intent(AdMobMainActivity.this, FullScreenActivityAdMob.class));
            finish();

        } else if (i == R.id.rewardedAdMobPage) {
            startActivity(new Intent(AdMobMainActivity.this, RewardedActivityAdMob.class));
            finish();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            startActivity(new Intent(AdMobMainActivity.this, Class.forName("megaapp.qa.appnext.com.megatestapp.MainActivity")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finish();
    }
}
