package megatestapp.qa.appnext.com.adapters.configurationadmob;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
import megatestapp.qa.appnext.com.adapters.admob.bannerssizes.AdMobBannersWithSetters;
import megatestapp.qa.appnext.com.adapters.R;

public class BannersConfigAdMob extends AppCompatActivity  implements View.OnClickListener{

    private EditText postBackTextAdMob;
    private EditText categoriesAdMob;
    private EditText videoLengthAdMob;
    private EditText videoMinLengthAdMob;
    private EditText videoMaxLengthAdMob;
    private Spinner creativeTypeSpinnerAdMob;
    private Switch autoPlaySwitchAdMob;
    private Switch muteSwitchAdMob;
    private SharedPreferences spSettersAdMob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_config_ad_mob);

        spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);

        String callingActivity = spSettersAdMob.getString("whoAmI", "small");
        // String callingActivity = getIntent().getStringExtra("bannerType");

        Button okBtnAdMob = findViewById(R.id.okBtnAdMob);
        okBtnAdMob.setOnClickListener(this);

        postBackTextAdMob = findViewById(R.id.postBackTextAdMob);
        categoriesAdMob = findViewById(R.id.categoriesTextAdMob);

        videoLengthAdMob = findViewById(R.id.videoLengthETAdMob);
        videoMinLengthAdMob = findViewById(R.id.minVideoLengthETAdMob);
        videoMaxLengthAdMob = findViewById(R.id.maxVideoLengthETAdMob);

        muteSwitchAdMob = findViewById(R.id.muteSwitchAdMob);
        muteSwitchAdMob.setChecked(true);
        muteSwitchAdMob.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        autoPlaySwitchAdMob = findViewById(R.id.autoPlaySwitchAdMob);
        autoPlaySwitchAdMob.setChecked(false);
        autoPlaySwitchAdMob.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            boolean selectionB;
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectionB = autoPlaySwitchAdMob.isChecked();
                // Toast.makeText(CustomEventInterstitial.this, String.valueOf(selectionB), Toast.LENGTH_SHORT).show();
                // do something, the isChecked will be
                // true if the switch is in the On position
            }
        });

        creativeTypeSpinnerAdMob = findViewById(R.id.creativeTypeSpinnerAdMob);
        creativeTypeSpinnerAdMob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        defineOrigin(callingActivity);
    }

    public void defineOrigin(String callingActivity){

        if(callingActivity.equals("small") || callingActivity.equals("large")){
            muteSwitchAdMob.setEnabled(false);
            autoPlaySwitchAdMob.setEnabled(false);
            creativeTypeSpinnerAdMob.setEnabled(false);
            videoLengthAdMob.setEnabled(false);
            videoMaxLengthAdMob.setEnabled(false);
            videoMinLengthAdMob.setEnabled(false);
        }
    }

    public void settersPerBanner(String callingActivity) {
        spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);
        String creativeTypeSelected = creativeTypeSpinnerAdMob.getSelectedItem().toString();
        spSettersAdMob.edit()
                .putString("category", categoriesAdMob.getText().toString())
                .putString("postBack", postBackTextAdMob.getText().toString())
                .putString("videoLength", videoLengthAdMob.getText().toString()).apply();
        if (videoMinLengthAdMob.getText().toString().isEmpty()){
            spSettersAdMob.edit().putInt("videoMin", 5).apply();
        }else{
            spSettersAdMob.edit().putInt("videoMin", Integer.parseInt(videoMinLengthAdMob
                    .getText().toString())).apply();
        }

        if (videoMaxLengthAdMob.getText().toString().isEmpty()){
            spSettersAdMob.edit().putInt("videoMax", 20).apply();
        }else{
            spSettersAdMob.edit().putInt("videoMax", Integer.parseInt(videoMaxLengthAdMob
                    .getText().toString())).apply();
        }

        spSettersAdMob.edit().putBoolean("mute", muteSwitchAdMob.isChecked())
                .putBoolean("autoPlay", autoPlaySwitchAdMob.isChecked())
                .putString("creativeType", creativeTypeSelected)
                .apply();
    }

    @Override
    public void onClick(View view) {
        spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);
        settersPerBanner(spSettersAdMob.getString("whoAmI", "small"));
        startActivity(new Intent(BannersConfigAdMob.this, AdMobBannersWithSetters.class)
                .putExtra("showBtn", true)
                .putExtra("calling-activity", spSettersAdMob.getString("whoAmI", "small")));
        finish();
    }

    @Override
    public void onBackPressed(){
        Toast.makeText(this, "Click 'Save' Button", Toast.LENGTH_SHORT).show();
    }
}
