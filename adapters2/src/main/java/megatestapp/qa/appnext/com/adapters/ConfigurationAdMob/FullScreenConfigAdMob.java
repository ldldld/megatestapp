package megatestapp.qa.appnext.com.adapters.configurationadmob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import megatestapp.qa.appnext.com.adapters.R;

public class FullScreenConfigAdMob extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_config_ad_mob);
    }
}
