package megatestapp.qa.appnext.com.adapters.mopub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.mopub.banners.AppnextMoPubCustomEventBanner;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import java.util.HashMap;
import java.util.Map;
import megatestapp.qa.appnext.com.adapters.R;
import megatestapp.qa.appnext.com.adapters.utilities.LogToastUtil;
import static com.appnext.banners.BannerAdRequest.TYPE_STATIC;
import static com.appnext.banners.BannerAdRequest.VIDEO_LENGTH_LONG;

public class BannersMoPub extends AppCompatActivity implements View.OnClickListener{
    private MoPubView moPubView;
    private Map<String, Object> extras;
    private String bannerSizeSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_mo_pub);
        moPubView = findViewById(R.id.adview);
        Button bannerDefaultLoad = findViewById(R.id.loadDefaultBanner);
        bannerDefaultLoad.setOnClickListener(this);

        extras = new HashMap<>();
        Button bannerCustomLoad = findViewById(R.id.loadCustomBanner);
        bannerCustomLoad.setOnClickListener(this);

        Spinner bannerSizeSpinner = findViewById(R.id.bannerSizeSpinner);
        bannerSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bannerSizeSelected = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (bannerSizeSelected){
            case("Small"):
                // BANNER
                //moPubView.setAdUnitId("6868416ab5a24ecbae1a64645fa02414");
                //moPubView.setAdUnitId("9f594cab40cf4a1faacbb542b0c525f4");
                int i = v.getId();
                if (i == R.id.loadDefaultBanner) {
                    moPubView.setAdUnitId("6868416ab5a24ecbae1a64645fa02414");
                    setCallBacks(moPubView, "small");
                    moPubView.loadAd();
                } else if (i == R.id.loadCustomBanner) {
                    moPubView.setAdUnitId("6868416ab5a24ecbae1a64645fa02414");
                    BannerAdRequest config = new BannerAdRequest()
                            .setCategories("omri,leon")
                            .setPostback("Postback string $$$");
                    extras.put(AppnextMoPubCustomEventBanner.AppnextConfigurationExtraKey, config);
                    extras.put("AppnextLanguage", "KO");
                    moPubView.setLocalExtras(extras);
                    setCallBacks(moPubView, "small");                }
                    moPubView.loadAd();
                break;
            case("Medium"):
                int j = v.getId();
                if (j == R.id.loadDefaultBanner) {
                    moPubView.setAdUnitId("29b78be80696444baf8ae0c2d90bbae6");
                    // 6868416ab5a24ecbae1a64645fa02414 new
                    // 0470626148cc4bf88f1bc0f9d3483a40 Evgeny
                    // 58fec6b4a6d54b8fb7bf0df428d43734 Eden
                    // 9f594cab40cf4a1faacbb542b0c525f4
                    setCallBacks(moPubView, "medium");
                    moPubView.loadAd();
                } else if (j == R.id.loadCustomBanner) {
                    moPubView.setAdUnitId("29b78be80696444baf8ae0c2d90bbae6");
                    // 6868416ab5a24ecbae1a64645fa02414 new
                    // 0470626148cc4bf88f1bc0f9d3483a40 Evgeny
                    // 58fec6b4a6d54b8fb7bf0df428d43734 Eden
                    BannerAdRequest config = new BannerAdRequest()
                            .setCategories("omri,leon")
                            .setPostback("Postback string $$$")
                            .setCreativeType(TYPE_STATIC)
                            .setAutoPlay(false)
                            .setMute(false)
                            .setVideoLength(VIDEO_LENGTH_LONG)
                            .setClickEnabled(false);
                    extras.put(AppnextMoPubCustomEventBanner.AppnextConfigurationExtraKey, config);
                    extras.put("AppnextLanguage", "KO");

                    moPubView.setLocalExtras(extras);
                    setCallBacks(moPubView, "medium");
                    moPubView.loadAd();
                }
                break;

            case("Large"):
                int k = v.getId();
                if (k == R.id.loadDefaultBanner) {
                    moPubView.loadAd();
                } else if (k == R.id.loadCustomBanner) {
                    BannerAdRequest config = new BannerAdRequest()
                            .setCategories("omri, leon")
                            .setPostback("Postback string $$$");
                    extras.put(AppnextMoPubCustomEventBanner.AppnextConfigurationExtraKey, config);
                    extras.put("AppnextLanguage", "KO");
                    moPubView.setLocalExtras(extras);
                    setCallBacks(moPubView, "large");
                    moPubView.loadAd();
                }
                setCallBacks(moPubView, "large");
                moPubView.setAdUnitId("0c620ef61d2944609b1513eaca6e4e49");
                break;

            case("Banner Size"):
                Toast.makeText(this, "Select Banner Size", Toast.LENGTH_SHORT).show();
                break;
        }
    }
//0c620ef61d2944609b1513eaca6e4e49
    private void setCallBacks(MoPubView moPubBannerEvent, final String bannerSize) {
        moPubBannerEvent.setBannerAdListener(new MoPubView.BannerAdListener() {
            @Override
            public void onBannerLoaded(MoPubView banner) {
                LogToastUtil.logToast(BannersMoPub.this, "Banner MoPub - " + bannerSize + " loaded");
                //Log.i("Banner MoPub - ", bannerSize + " loaded");
            }

            @Override
            public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
                LogToastUtil.logToast(BannersMoPub.this, "Banner MoPub - " + bannerSize + " failed");
                //Log.i("Baner MoPub - ", bannerSize + " failed");
            }

            @Override
            public void onBannerClicked(MoPubView banner) {
                LogToastUtil.logToast(BannersMoPub.this, "Banner MoPub - " + bannerSize + " clicked");
                //                Log.i("Banner MoPub - ", bannerSize + " clicked");
            }

            @Override
            public void onBannerExpanded(MoPubView banner) {
                LogToastUtil.logToast(BannersMoPub.this, "Banner MoPub - " + bannerSize + " expanded");
                //Log.i("Banner MoPub - ", bannerSize + " expanded");
            }

            @Override
            public void onBannerCollapsed(MoPubView banner) {
                LogToastUtil.logToast(BannersMoPub.this, "Banner MoPub - " + bannerSize + " collapsed");
                //Log.i("Banner MoPub - ", bannerSize + " collapsed");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearMemory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearMemory();
        startActivity(new Intent(BannersMoPub.this, MoPubMainActivity.class));
        finish();
    }

    public void clearMemory(){
        if (moPubView != null) {
            moPubView.destroy();
        }
        if (extras != null)
            extras.clear();
    }
}
