package megatestapp.qa.appnext.com.adapters.mopub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.appnext.core.AppnextError;
import com.appnext.nativeads.NativeAd;
import com.appnext.sdk.adapters.mopub.nativeads.native_ads2.AppnextMoPubCustomEventNative;
import com.appnext.sdk.adapters.mopub.nativeads.native_ads2.AppnextMopubCallbacks;
import com.mopub.nativeads.MediaViewBinder;
import com.mopub.nativeads.MoPubAdAdapter;
import com.mopub.nativeads.MoPubNativeAdLoadedListener;
import com.mopub.nativeads.MoPubNativeAdPositioning;
import com.mopub.nativeads.MoPubStaticNativeAdRenderer;
import com.mopub.nativeads.MoPubVideoNativeAdRenderer;
import com.mopub.nativeads.RequestParameters;
import com.mopub.nativeads.ViewBinder;
import java.util.Map;
import megatestapp.qa.appnext.com.adapters.R;
import megatestapp.qa.appnext.com.adapters.utilities.LogToastUtil;


public class NativeAdsMoPub extends AppCompatActivity{
    private MoPubAdAdapter mAdAdapter;

//        private MoPubSampleAdUnit mAdConfiguration;
    private final static String adUnitId = "468dae83ca9444e1aee854297380fcd2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_ads_mo_pub);
        //        final View contentView = findViewById(R.id.content_view);
        ListView listView = findViewById(R.id.native_list_view);
//        final DetailFragmentViewHolder views = DetailFragmentViewHolder.fromView(contentView);
//        final String adUnitId = "cc16e61ef660421490b3e3981ce1ef32";

        //  final String adUnitId = "dce44a03b9064f65b619f5ee54a3b033";
//   817053b638cd4525aea16a4e2a32ffc0 - native Ads
//   468dae83ca9444e1aee854297380fcd2 - native ads test
        // a0abf65f94194f72bab0dfb71c5fab16 - native ads test 2
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1);
        for (int i = 0; i < 100; ++i) {
            adapter.add("Item " + i);
        }

        // Create an ad adapter that gets its positioning information from the MoPub Ad Server.
        // This adapter will be used in place of the original adapter for the ListView.
        mAdAdapter = new MoPubAdAdapter(this, adapter, new MoPubNativeAdPositioning.MoPubServerPositioning());

        // Set up a renderer that knows how to put ad data in your custom native view.
        final MoPubStaticNativeAdRenderer staticAdRender = new MoPubStaticNativeAdRenderer(
                new ViewBinder.Builder(R.layout.mopub_native_ad_list_item)
                        .privacyInformationIconImageId(R.id.privacyPolicyIcon)
                        .titleId(R.id.native_ad_title)
                        .textId(R.id.native_ad_text)
                        .mainImageId(R.id.native_ad_main_image)
                        .iconImageId(R.id.native_ad_daa_icon_image)
                        .callToActionId(R.id.native_cta)
                        .build());

        // Set up a renderer for a video native ad.
        MoPubVideoNativeAdRenderer videoAdRenderer = new MoPubVideoNativeAdRenderer(
                new MediaViewBinder.Builder(R.layout.mopub_native_video_ad_list_item)
                        .privacyInformationIconImageId(R.id.privacyPolicyIcon)
                        .titleId(R.id.native_ad_title)
                        .textId(R.id.native_ad_text)
                        .mediaLayoutId(R.id.native_ad_main_image)
                        .iconImageId(R.id.native_ad_daa_icon_image)
                        .callToActionId(R.id.native_cta)
                        .build());

        // Register the renderers with the MoPubAdAdapter and then set the adapter on the ListView.

        mAdAdapter.registerAdRenderer(videoAdRenderer);
        mAdAdapter.registerAdRenderer(staticAdRender);
        listView.setAdapter(mAdAdapter);
        //listView.removeView(listView);
        RequestParameters mRequestParameters = null ;
        mAdAdapter.loadAds(adUnitId, mRequestParameters);


        mAdAdapter.setAdLoadedListener(new MoPubNativeAdLoadedListener() {
            @Override
            public void onAdLoaded(int position) {
                LogToastUtil.logToast(NativeAdsMoPub.this, "Native Ads MoPub - Loaded");            }
            @Override
            public void onAdRemoved(int position) {
                LogToastUtil.logToast(NativeAdsMoPub.this, "Native Ads MoPub - Removed");            }
        });

        AppnextMoPubCustomEventNative.registerNativeAdListener(new AppnextMopubCallbacks(){
            @Override
            public void onAdLoaded(NativeAd nativeAd, Map<String, Object> localExtras, Map<String, String> serverExtras) {
                LogToastUtil.logToast(NativeAdsMoPub.this, "Native Ads MoPub - Loaded Custom Event");
            }

            @Override
            public void onAdClicked(NativeAd nativeAd, Map<String, Object> localExtras, Map<String, String> serverExtras) {
                LogToastUtil.logToast(NativeAdsMoPub.this, "Native Ads MoPub - Clicked");
            }

            @Override
            public void onError(NativeAd nativeAd, AppnextError error, Map<String, Object> localExtras, Map<String, String> serverExtras) {
                LogToastUtil.logToast(NativeAdsMoPub.this, "Native Ads MoPub - Error " + error.getErrorMessage());

            }

            @Override
            public void adImpression(NativeAd nativeAd, Map<String, Object> localExtras, Map<String, String> serverExtras) {
                LogToastUtil.logToast(NativeAdsMoPub.this, "Native Ads MoPub - Impressed " + nativeAd.getAdTitle());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppnextMoPubCustomEventNative.registerNativeAdListener(null);
        if(mAdAdapter != null){
            mAdAdapter.destroy();
        }
        startActivity(new Intent(NativeAdsMoPub.this, MoPubMainActivity.class));
        finish();
    }
}
