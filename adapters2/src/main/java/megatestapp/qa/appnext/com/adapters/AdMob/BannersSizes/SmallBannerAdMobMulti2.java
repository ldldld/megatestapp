package megatestapp.qa.appnext.com.adapters.admob.bannerssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.admob.banners.AppnextAdMobBannerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import megatestapp.qa.appnext.com.adapters.admob.BannersAdMob;
import megatestapp.qa.appnext.com.adapters.R;

public class SmallBannerAdMobMulti2 extends AppCompatActivity implements View.OnClickListener {
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_small_banner_ad_mob_multi2);

        Button loadDefaultSmallBanner = findViewById(R.id.loadDefaultSmallBanner);
        loadDefaultSmallBanner.setOnClickListener(this);

        Button loadCustomSmallBanner = findViewById(R.id.loadCustomSmallBanner);
        loadCustomSmallBanner.setOnClickListener(this);

        Button smallBannerSetters = findViewById(R.id.smallBannerSetters);
        smallBannerSetters.setOnClickListener(this);
        mAdView = findViewById(R.id.adViewSmall);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadDefaultSmallBanner) {
            if (mAdView != null) {
                mAdView.destroy();
            }
            BannerAdRequest configSmallDef = new BannerAdRequest();
            Bundle extras = new Bundle();
            extras.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configSmallDef);
            AdRequest adRequestDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extras).build();

            callBacks();
            mAdView.loadAd(adRequestDefault);

        } else if (i == R.id.loadCustomSmallBanner) {
            if (mAdView.isLoading()) {
                mAdView.destroy();
            }
            BannerAdRequest configSmallCus = new BannerAdRequest();
            Bundle extrasSmall = new Bundle();
            configSmallCus.setCategories("Games, Adventure")
                    .setPostback("AdMob Installed");
            extrasSmall.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configSmallCus);
            AdRequest adRequestCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extrasSmall).build();
            callBacks();
            mAdView.loadAd(adRequestCustom);


        }else if (i == R.id.smallBannerSetters){
            SharedPreferences spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);
            spSettersAdMob.edit().putString("whoAmI", "small").apply();
            startActivity(new Intent(SmallBannerAdMobMulti2.this,
                    AdMobBannersWithSetters.class));


        }
    }

    public void callBacks() {
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Toast.makeText(SmallBannerAdMobMulti2.this, "Banner Loaded", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.

                Toast.makeText(SmallBannerAdMobMulti2.this, "Banner Load Error", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Toast.makeText(SmallBannerAdMobMulti2.this, "Banner Opened", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Toast.makeText(SmallBannerAdMobMulti2.this, "Banner Left App", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                Toast.makeText(SmallBannerAdMobMulti2.this, "Banner Closed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mAdView != null) {
            mAdView.destroy();
        }
        startActivity(new Intent(SmallBannerAdMobMulti2.this, BannersAdMob.class));
        finish();
    }
}
