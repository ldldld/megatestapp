package megatestapp.qa.appnext.com.adapters.admob.bannerssizes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.appnext.banners.BannerAdRequest;
import com.appnext.banners.BannerListener;
import com.appnext.banners.BannerView;
import com.appnext.banners.BannersError;
import com.appnext.core.AppnextError;
import megatestapp.qa.appnext.com.adapters.admob.BannersAdMob;
import megatestapp.qa.appnext.com.adapters.configurationadmob.BannersConfigAdMob;
import megatestapp.qa.appnext.com.adapters.R;

public class AdMobBannersWithSetters extends AppCompatActivity implements View.OnClickListener{
    private SharedPreferences spSettersAdMob;
    private BannerView bannerViewAdMob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_mob_banners_with_setters);

        Button settersBtnAdMob = findViewById(R.id.settersBtnAdMob);
        settersBtnAdMob.setOnClickListener(this);

//        showDefaultBtnAll = (Button) findViewById(R.id.showDefaultBtnAll);
//        showDefaultBtnAll.setOnClickListener(this);

        Button showCustomBtnAllAdMob = findViewById(R.id.showCustomBtnAllAdMob);
        showCustomBtnAllAdMob.setOnClickListener(this);

        spSettersAdMob = getSharedPreferences("settersAdMob", MODE_PRIVATE);

        showCustomBtnAllAdMob.setEnabled(false);

        String callingActivity = spSettersAdMob.getString("whoAmI", "small");
        switch (callingActivity){
            case("small"):
                // showDefaultBtnAll.setText("Show Default Small");
                showCustomBtnAllAdMob.setText("Show Custom Small");
                break;

            case("medium"):
                // showDefaultBtnAll.setText("Show Default Medium");
                showCustomBtnAllAdMob.setText("Show Custom Medium");
                break;

            case("large"):
                // showDefaultBtnAll.setText("Show Default Large");
                showCustomBtnAllAdMob.setText("Show Custom Large");
                break;
        }
        Intent in = getIntent();
        if (in.getStringExtra("calling-activity") != null) {
            showCustomBtnAllAdMob.setEnabled(true);
        }
       // Appnext.init(this);
    }

    @Override
    public void onClick(final View v) {
        int i = v.getId();
        if (i == R.id.settersBtnAdMob){
            startActivity(new Intent(AdMobBannersWithSetters.this, BannersConfigAdMob.class));
            finish();
        }else if (i == R.id.showCustomBtnAllAdMob){
//            BannerView customBanner = new BannerView(this);
            //customBanner.setPlacementId(PLACEMENT_ID);
            //customBanner.setBannerSize(BannerSize.BANNER);
            BannerAdRequest banner_requestCustom = new BannerAdRequest();
            switch (spSettersAdMob.getString("creativeType", BannerAdRequest.TYPE_ALL)){

                case ("All"):
                    banner_requestCustom.setCreativeType(BannerAdRequest.TYPE_ALL);
                    break;

                case ("Static"):
                    banner_requestCustom.setCreativeType(BannerAdRequest.TYPE_STATIC);
                    break;

                case ("Video"):
                    banner_requestCustom.setCreativeType(BannerAdRequest.TYPE_VIDEO);
                    break;
            }
            banner_requestCustom
                    .setCategories(spSettersAdMob.getString("category", "Games"))
                    .setPostback(spSettersAdMob.getString("postBack", "App Download"))
                    .setAutoPlay(spSettersAdMob.getBoolean("autoPlay", true))
                    .setMute(spSettersAdMob.getBoolean("mute", true))
                    .setVideoLength(spSettersAdMob.getString("videoLength", "15"))
                    //.setVideoLength("LONG")
                    .setVidMin(spSettersAdMob.getInt("videoMin", 5))
                    .setVidMax(spSettersAdMob.getInt("videoMax", 10));
            if ((spSettersAdMob.getString("whoAmI", "small")).equals("small")){
                //customBanner.setBannerSize(BannerSize.BANNER);
                bannerViewAdMob = findViewById(R.id.smallBannerAdMob);
                bannerViewAdMob.loadAd(banner_requestCustom);
            }else{
                if ((spSettersAdMob.getString("whoAmI", "small")).equals("medium")){
                    // customBanner.setBannerSize(BannerSize.MEDIUM_RECTANGLE);
                    bannerViewAdMob = findViewById(R.id.mediumBannerAdMob);
                    bannerViewAdMob.loadAd(banner_requestCustom);
                }else{
                    if ((spSettersAdMob.getString("whoAmI", "small")).equals("large")){
                        // customBanner.setBannerSize(BannerSize.LARGE_BANNER);
                        bannerViewAdMob = findViewById(R.id.largeBannerAdMob);
                        bannerViewAdMob.loadAd(banner_requestCustom);
                    }
                }
            }
            callBacks();
        }
    }

    public void callBacks(){
        bannerViewAdMob.setBannerListener(new BannerListener() {
            @Override
            public void onError(AppnextError s) {
                super.onError(s);
                switch (s.getErrorMessage()) {
                    case BannersError.CONNECTION_ERROR:
                        Log.v("appnext", "client connection error");
                        break;
                    case BannersError.NO_MARKET:
                        Log.v("appnext", "couldn't open a valid market");
                        break;

                    case BannersError.TIMEOUT:
                        Log.v("appnext", "connection time out client");
                        break;

                    case BannersError.NO_ADS:
                        Log.v("appnext", "server - no ads");
                        break;

                    case BannersError.INTERNAL_ERROR:
                        Log.v("appnext", "Internal error");
                        break;

                    case BannersError.NULL_CONTEXT:
                        Log.v("appnext", "Null Context");
                        break;

                    case BannersError.SLOW_CONNECTION:
                        Log.v("appnext", "Slow Connection");
                        break;

                    default:
                        Log.v("appnext", "other error");
                        break;
                }

                Toast.makeText(AdMobBannersWithSetters.this, "Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded(String s) {
                super.onAdLoaded(s);
                Toast.makeText(AdMobBannersWithSetters.this, "Ad Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void adImpression() {
                super.adImpression();
                Toast.makeText(AdMobBannersWithSetters.this, "Ad Impression", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
                Toast.makeText(AdMobBannersWithSetters.this, "Ad Clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bannerViewAdMob != null) {
            bannerViewAdMob.destroy();
        }
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(bannerViewAdMob != null) {
            bannerViewAdMob.destroy();
        }
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        if(bannerViewAdMob != null) {
            bannerViewAdMob.destroy();
        }
        startActivity(new Intent(AdMobBannersWithSetters.this, BannersAdMob.class));
        finish();
    }
}
