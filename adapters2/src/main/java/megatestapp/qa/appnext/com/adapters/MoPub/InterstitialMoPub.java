package megatestapp.qa.appnext.com.adapters.mopub;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.core.Ad;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import java.util.HashMap;
import java.util.Map;
import megatestapp.qa.appnext.com.adapters.R;
import megatestapp.qa.appnext.com.adapters.utilities.LogToastUtil;

public class InterstitialMoPub extends AppCompatActivity implements View.OnClickListener{
    private Button showDefaultInterstitial;
    private Button showCustomInterstitial;
    private MoPubInterstitial defaultEventInterstitial;
    private MoPubInterstitial customEventInterstitial;
    private final static String MO_PUB_AD_UNIT_ID = "0d01a6e49ee04123bbe8f191b73c5ea7";
    //private final static String MO_PUB_AD_UNIT_ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial_mo_pub);

        Button loadDefaultInterstitial = findViewById(R.id.loadDefaultInterstitial);
        loadDefaultInterstitial.setOnClickListener(this);

        showDefaultInterstitial = findViewById(R.id.showDefaultInterstitial);
        showDefaultInterstitial.setOnClickListener(this);
        showDefaultInterstitial.setEnabled(false);

        Button loadCustomInterstitial = findViewById(R.id.loadCustomInterstitial);
        loadCustomInterstitial.setOnClickListener(this);

        showCustomInterstitial = findViewById(R.id.showCustomInterstitial);
        showCustomInterstitial.setOnClickListener(this);
        showCustomInterstitial.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.loadDefaultInterstitial) {
            if (defaultEventInterstitial != null) {
                defaultEventInterstitial.destroy();
            }
            defaultEventInterstitial = new MoPubInterstitial(this, MO_PUB_AD_UNIT_ID);
            callBacks(defaultEventInterstitial, "default");
            defaultEventInterstitial.load();

        } else if (i == R.id.showDefaultInterstitial) {
            defaultEventInterstitial.show();

        } else if (i == R.id.loadCustomInterstitial) {
            if (customEventInterstitial != null) {
                customEventInterstitial.destroy();
            }
            customEventInterstitial = new MoPubInterstitial(this, MO_PUB_AD_UNIT_ID);
            Map<String, Object> extrasInterstitial = new HashMap();
            InterstitialConfig interstitialConfig = new InterstitialConfig();
            interstitialConfig.setButtonColor("#6AB344");
            interstitialConfig.setPostback("Your_Postback_Params");
            interstitialConfig.setCategories("Categories");
            interstitialConfig.setSkipText("Skip");
            interstitialConfig.setMute(false);
            interstitialConfig.setAutoPlay(true);
            interstitialConfig.setCreativeType(Interstitial.TYPE_MANAGED);
            interstitialConfig.setOrientation(Ad.ORIENTATION_AUTO);
            interstitialConfig.setLanguage("KO");
            extrasInterstitial.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, interstitialConfig);
            customEventInterstitial.setLocalExtras(extrasInterstitial);
            callBacks(customEventInterstitial, "custom");
            customEventInterstitial.load();

        } else if (i == R.id.showCustomInterstitial) {
            customEventInterstitial.show();

        }
    }
    public void callBacks(MoPubInterstitial interstitialEvent, final String eventType){
        interstitialEvent.setInterstitialAdListener(new MoPubInterstitial.InterstitialAdListener() {
            @Override
            public void onInterstitialLoaded(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultInterstitial.setEnabled(true);
                }else{
                    showCustomInterstitial.setEnabled(true);
                }
                LogToastUtil.logToast(InterstitialMoPub.this, "Interstitial MoPub - Loaded");

            }

            @Override
            public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
                LogToastUtil.logToast(InterstitialMoPub.this, "Interstitial MoPub Error - " + errorCode.toString());


            }

            @Override
            public void onInterstitialShown(MoPubInterstitial interstitial) {

                if(eventType.equals("default")){
                    showDefaultInterstitial.setEnabled(false);
                }else{
                    showCustomInterstitial.setEnabled(false);
                }

                LogToastUtil.logToast(InterstitialMoPub.this, "Interstitial MoPub - Shown");
            }

            @Override
            public void onInterstitialClicked(MoPubInterstitial interstitial) {
                LogToastUtil.logToast(InterstitialMoPub.this, "Interstitial MoPub - Clicked");
            }

            @Override
            public void onInterstitialDismissed(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultInterstitial.setEnabled(false);
                }else{
                    showCustomInterstitial.setEnabled(false);
                }
                LogToastUtil.logToast(InterstitialMoPub.this, "Interstitial MoPub - Dismissed");
            }
        });
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        if (defaultEventInterstitial != null) {
//            defaultEventInterstitial.destroy();
//        }
//        if (customEventInterstitial != null) {
//            customEventInterstitial.destroy();
//        }
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (defaultEventInterstitial != null) {
            defaultEventInterstitial.destroy();
        }
        if (customEventInterstitial != null) {
            customEventInterstitial.destroy();
        }
        startActivity(new Intent(InterstitialMoPub.this, MoPubMainActivity.class));
        finish();
    }
}
