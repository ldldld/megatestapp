package megatestapp.qa.appnext.com.adapters.mopub;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mopub.common.MediationSettings;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentDialogListener;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.MoPubErrorCode;

import java.util.LinkedList;
import java.util.List;
import megatestapp.qa.appnext.com.adapters.R;

import static com.mopub.common.MoPub.getPersonalInformationManager;
import static com.mopub.common.MoPub.isSdkInitialized;

public class MoPubMainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final List<String> sNetworksToInit = new LinkedList<>();
    private static SdkConfiguration sdkConfiguration;
    private PersonalInfoManager mPersonalInfoManager;
    private SdkInitializationListener sdkInitializationListener;
    private MediationSettings[] mMediationSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mo_pub_main);
        Button goToBanners = findViewById(R.id.bannersPage);
        goToBanners.setOnClickListener(this);
        Button goToInterstitial = findViewById(R.id.interstitialPage);
        goToInterstitial.setOnClickListener(this);
        Button goToFullScreen = findViewById(R.id.fullScreenPage);
        goToFullScreen.setOnClickListener(this);
        Button goToRewarded = findViewById(R.id.rewardedPage);
        goToRewarded.setOnClickListener(this);
        Button nativeMoPubAdsPage = findViewById(R.id.nativeMoPubAdsPage);
        nativeMoPubAdsPage.setOnClickListener(this);

        mMediationSettings = new MediationSettings[0];
        sdkInitializationListener = new SdkInitializationListener() {
            @Override
            public void onInitializationFinished() {
                if (mPersonalInfoManager != null) {
                    if (mPersonalInfoManager.shouldShowConsentDialog()){
                        mPersonalInfoManager.loadConsentDialog(new ConsentDialogListener(){

                            @Override
                            public void onConsentDialogLoaded() {
                                if (mPersonalInfoManager != null) {
                                    mPersonalInfoManager.showConsentDialog();
                                }
                            }

                            @Override
                            public void onConsentDialogLoadFailed(@NonNull MoPubErrorCode moPubErrorCode) {
                                MoPubLog.i("Consent dialog failed to load.");
                            }
                        });
                    }
                }
            }
        };

        if (sdkConfiguration == null) {
            sdkConfiguration = new SdkConfiguration.Builder("b195f8dd8ded45fe847ad89ed1d016da")
                    .withMediationSettings(mMediationSettings)
                    .withNetworksToInit(sNetworksToInit)
                    .build();
            Toast.makeText(this, "Created new", Toast.LENGTH_SHORT).show();
            MoPub.initializeSdk(this, sdkConfiguration, sdkInitializationListener);
            mPersonalInfoManager = getPersonalInformationManager();
            mPersonalInfoManager.loadConsentDialog(new ConsentDialogListener() {

                @Override
                public void onConsentDialogLoaded() {
                    Toast.makeText(MoPubMainActivity.this, "Consent Loaded", Toast.LENGTH_SHORT).show();
                    if (mPersonalInfoManager != null) {
                        mPersonalInfoManager.showConsentDialog();
                    }
                }

                @Override
                public void onConsentDialogLoadFailed(@NonNull MoPubErrorCode moPubErrorCode) {
                    Toast.makeText(MoPubMainActivity.this, "Consent NOT Loaded", Toast.LENGTH_SHORT).show();
                    MoPubLog.i("Consent dialog failed to load.");

                }
            });

        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.bannersPage) {
            startActivity(new Intent(MoPubMainActivity.this, BannersMoPub.class));
            finish();

        } else if (i == R.id.interstitialPage) {
            startActivity(new Intent(MoPubMainActivity.this, InterstitialMoPub.class));
            finish();

        } else if (i == R.id.fullScreenPage) {
            startActivity(new Intent(MoPubMainActivity.this, FullScreenActivityMoPub.class));
            finish();

        } else if (i == R.id.rewardedPage) {
            startActivity(new Intent(MoPubMainActivity.this, RewardedMoPub.class));
            finish();

        } else if (i == R.id.nativeMoPubAdsPage) {
            startActivity(new Intent(MoPubMainActivity.this, NativeAdsMoPub.class));
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        try {
            sdkConfiguration = null;
            startActivity(new Intent(MoPubMainActivity.this, Class.forName("megaapp.qa.appnext.com.megatestapp.MainActivity")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finish();
    }

    // MoPub initialization CallBack
//    @Override
//    public void onInitializationFinished() {
//
//        if (mPersonalInfoManager != null) {
//            if (mPersonalInfoManager.shouldShowConsentDialog()){
//                mPersonalInfoManager.loadConsentDialog(new ConsentDialogListener(){
//
//                    @Override
//                    public void onConsentDialogLoaded() {
//                        if (mPersonalInfoManager != null) {
//                            mPersonalInfoManager.showConsentDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onConsentDialogLoadFailed(@NonNull MoPubErrorCode moPubErrorCode) {
//                        MoPubLog.i("Consent dialog failed to load.");
//
//                    }
//                });
//
//            }
//        }
//        if(isSdkInitialized()){
//
//            Toast.makeText(this, "MoPub initialized", Toast.LENGTH_SHORT).show();
//        }
//    }
}
